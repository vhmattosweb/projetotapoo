from WebScraper_v3.src.SiteScraper import FinaSite


if __name__ == "__main__":
    fina_site = FinaSite("https://www.worldaquatics.com/competitions/2943/olympic-games-paris-2024")
    fina_site.launch_scraper()
    fina_site.save_result("/home/matheus/ProjetoTAPOO/database/data/FINA.json")
