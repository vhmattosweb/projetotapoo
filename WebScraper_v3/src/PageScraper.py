"""
Here, we define the interface and concrete implementations that handle data extraction from websites.
The desired content may not reside on the same page but can be found in other links within the page.
To extract the data of the linked pages, we use Chain of Responsability.
"""

from __future__ import annotations
from abc import ABC, abstractmethod
from WebScraper_v3.src.PageLoader import PageLoader, Loader
from WebScraper_v3.src.ScraperAlgorithm import Scraper


class PageScraper(ABC):
    """Interface to manage the data scraping of a specific page."""

    @abstractmethod
    def set_next_page(self, page_scraper: PageScraper):
        """Configures the PageScraper for web pages that are linked to the current one."""
        pass

    @abstractmethod
    def scrape_data(self, url: str):
        """Runs the data scraping algorithm."""
        pass


class AbstractPageScraper(PageScraper):
    """
    Abstract PageScraper implementation that manages common methods shared by all concrete implementations of
    the PageScraper interface.
    """

    def __init__(self, loader_strategy: Loader, scraper: Scraper):
        # stores the way we want to load the page
        self.page_loader = PageLoader(loader_strategy=loader_strategy)
        # scraper strategy we need to use
        self.__scraper = scraper
        # stores the next Page to be used when it's needed
        self._next_page_scraper = None

    @abstractmethod
    def scrape_data(self, url: str):
        """
        Default scraping method.
        It launches the Scraper object in the webpage alocated at url.
        """
        # making the request
        self.page_loader.make_request(url)
        # launching the scraper
        data_ = self.__scraper.launch_scraper(self.page_loader)
        # closing connection
        self.page_loader.close_page()
        # returning the scraped data
        return data_

    def set_next_page(self, page_scraper: PageScraper):
        """Sets the next PageScraper object that we need to delegate the scraping of the linked pages to."""
        self._next_page_scraper = page_scraper


class FinaEditionsPage(AbstractPageScraper):
    """Concrete implementation of PageScraper to extract all the data from the Olympic editions."""

    def scrape_data(self, url: str):
        # list of all editions
        editions = super().scrape_data(url)
        # dictionary to return all editions data
        editions_data = {}
        for edition, edition_link in editions.items():
            print(f"INFO: Scraping data from {edition}")
            # delegating the data scraping of the content in the links to the next_page_scraper
            editions_data[edition] = {"swimming": self._next_page_scraper.scrape_data(edition_link)}
        # return the editions
        return editions_data


class FinaModalitiesPage(AbstractPageScraper):
    """Concrete implementation of PageScraper to scrape modalities of a given edition."""

    def scrape_data(self, url: str):
        # dictionary to store the link of all modalities
        modalities_links, tries = {}, 0
        # here we need to try a couple of times to scraper the data
        while not len(modalities_links) and tries < 10:
            modalities_links = super().scrape_data(url)
            tries += 1
        # dictionary to store all the modalities
        modalities_data = {}
        # loop over the modalities
        for modality, modality_link in modalities_links.items():
            print(f"INFO: Scraping data from {modality}")
            modalities_data[modality] = self._next_page_scraper.scrape_data(modality_link)
        # return results from the modalities
        return modalities_data


class FinaMarksPage(AbstractPageScraper):
    """
    Concrete implementation of the PageScraper.
    It extracts the data from a .json file
    """
    def scrape_data(self, url: str):
        return super().scrape_data(url)
