"""
File that contains the definition for the SiteScraper interface.
This purpuse of this class is to scrape all data that we want from a specfic website.
"""

from abc import ABC, abstractmethod
from WebScraper_v3.src.ScraperAlgorithm import FinaEditionScraper, FinaAPIScraper, FinaModalityLinkScraper
from WebScraper_v3.src.PageScraper import FinaEditionsPage, FinaModalitiesPage, FinaMarksPage
from WebScraper_v3.src.PageLoader import DynamicLoader, StaticLoader
import json


class SiteScraper(ABC):
    """
    SiteScraper abstract class.
    Constructs the linked pages and launches the Scraper on a website.
    """

    def __init__(self, url: str):
        self._url = url
        self._page_scraper = None
        self.__data = {}

    @abstractmethod
    def create_linked_pages(self):
        """Creates the linked pages that will be used to extract the data from the website"""
        pass

    def launch_scraper(self):
        # creating the linked pages
        self.create_linked_pages()
        self.__data = self._page_scraper.scrape_data(self._url)

    def save_result(self, filename):
        with open(filename, "w") as file_:
            file_.write(json.dumps(self.__data, indent=4))


class FinaSite(SiteScraper):
    """
    Class to represent the fina web-site that we wish to scrape the data from.
    """

    def create_linked_pages(self):
        # loaders
        static_loader, dynamic_loader = StaticLoader(), DynamicLoader()

        # pages to handle the scraper
        self._page_scraper = FinaEditionsPage(static_loader, FinaEditionScraper())
        modality_scraper = FinaModalitiesPage(dynamic_loader, FinaModalityLinkScraper())
        marks_scraper = FinaMarksPage(static_loader, FinaAPIScraper())

        # linking the chain
        modality_scraper.set_next_page(marks_scraper)
        self._page_scraper.set_next_page(modality_scraper)
