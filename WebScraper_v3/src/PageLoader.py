"""
Classes responsible for loading and storing the content of a specific webpage.
"""

import requests
from selenium import webdriver
from abc import ABC, abstractmethod


class Loader(ABC):
    """Interface for loading a specific webpage"""

    @abstractmethod
    def load_page(self, url: str):
        pass


class DynamicLoader(Loader):
    """Concrete implementation of the Loader interface for cases where the content is dynamically generated."""

    def load_page(self, url: str):
        # accessing the page
        driver = webdriver.Firefox()
        driver.get(url)
        driver.implicitly_wait(50)
        # return content
        return driver


class StaticLoader(Loader):
    """Concrete implementation of the Loader interface for cases where the content is statically generated."""

    def load_page(self, url: str):
        # making the request
        request = requests.get(
            url,
            headers={"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 "
                                   "(KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36"},
        )
        # returning the request
        return request


class PageLoader:
    """Class responsible for storing the result of the content request for a specific Web page."""

    def __init__(self, loader_strategy: Loader):
        self.__request = None
        self.__loader_strategy: Loader = loader_strategy

    def make_request(self, url: str):
        self.__request = self.__loader_strategy.load_page(url)

    def close_page(self):
        self.__request.close()

    @property
    def request(self):
        """Returns the result of the request"""
        return self.__request
