"""
File that stores all the algorithm that we'll be used for extracting data.
"""

from WebScraper_v3.src.PageLoader import PageLoader
from bs4 import BeautifulSoup
from abc import ABC, abstractmethod


class Scraper(ABC):
    """
    Abstract class to store the scraping algorithm of a specific WebPage.
    """

    @classmethod
    @abstractmethod
    def launch_scraper(cls, page_loader: PageLoader):
        pass


class FinaEditionScraper(Scraper):
    """Class to scrape all editions from the FINA website"""

    # tags that will be used to identify the content
    tag, attrs = "a", {"class": ["other-years__competition-link", "link-underline-trigger"],
                       "title": "View competition"}

    @classmethod
    def launch_scraper(cls, page_loader: PageLoader):
        """Scrape all links to the Olympic editions from the FINA webpage"""
        # hold the link information
        soup = BeautifulSoup(page_loader.request.text, "html.parser")

        editions_dict = {}
        # loop over all links
        for edition_tag in soup.find_all(cls.tag, cls.attrs):
            # link
            edition_link = edition_tag.get("href")
            # skipping all non-olympics competetions
            if "olympic-games" not in edition_link:
                continue
            _, edition_name = edition_link.split("olympic-games-")
            # storing into the dictionary
            editions_dict[edition_name] = f"https://www.worldaquatics.com{edition_link}/results?disciplines=SW"

        # return results
        return editions_dict


class FinaModalityLinkScraper(Scraper):
    """Scrapes all links to the modality results"""

    tag, attrs = "button", {"class": ["results-table__event", "schedule__item-link", "js-results-event"]}
    internal_api = "https://api.worldaquatics.com/fina/events/"

    @classmethod
    def launch_scraper(cls, page_loader: PageLoader):
        # dictionary to hold all links
        links = dict()
        soup = BeautifulSoup(page_loader.request.page_source, "html.parser")
        # loop over all modalities
        for modality in soup.find_all(cls.tag, cls.attrs):
            # link
            data_event_guid = modality.get("data-event-guid")
            # getting the name
            modality_name = modality.find("h3", {"class": "schedule__item-title"}).text.lower()
            # skipping all relays
            if "relay" in modality_name:
                continue
            links[modality_name] = f"{cls.internal_api}{data_event_guid}"
        # return results
        return links


class FinaAPIScraper(Scraper):
    """Scrapes results from a table"""

    @classmethod
    def launch_scraper(cls, page_loader: PageLoader):
        # here we received a json file
        json_file = page_loader.request.json()
        # dictionary to store the marks
        marks = []
        # finding the final heat
        finals = [heat["Results"] for heat in json_file["Heats"] if "Final" in heat["Name"]]
        for final in finals:
            for mark in final:
                if "Time" in mark.keys():
                    marks.append(
                        {
                            "Athlete": f"{mark['FirstName'].lower()} {mark['LastName'].lower()}",
                            "Country": f"{mark['NAT']}",
                            "Result": f"{mark['Time']}"
                        }
                    )
        # return all marks
        return marks
