from abc import ABC, abstractmethod
from src.core_classes.Mark import Mark
from src.core_classes.Athlete import Athlete
from typing import List
from src.ModalityObserver import ModalityObserver


class SortStrategy(ABC):
    """
    Interface responsable to define algorithm to sort the Marks.
    """

    @abstractmethod
    def sort_marks(self, marks: List[Mark]) -> List[Mark]:
        """
        This method defines the strategy to sort the Marks objects in a list.

        :param marks:
            list of Marks objects we need to sort
        :return
            the sorted list of marks
        """
        pass


class SortByBestMark(SortStrategy):
    """
    Sort the marks using the Mark value.
    This strategy is used to construct the default ranking, which order the marks acording to their value.
    """

    def __init__(self):
        # reference to the Modality Observer object
        # this object is responsible to tell if the marks should be ordered in increasing order or decresing order
        # The first is for the modalities that have marks with time unit (100m free, 50m free)
        # the second happens for the modalities that have marks with distance units (high jump for example)
        self.__modality_obs = ModalityObserver()

    def sort_marks(self, marks: List[Mark]) -> List[Mark]:
        # if the units of the marks is distance, we need to reverse the order of the ranking
        reverse_order = self.__modality_obs.is_distance_unit()
        sorted_marks = sorted(marks, reverse=reverse_order)
        return sorted_marks


class SortByEdition(SortStrategy):
    """
    Sort the marks by Edition year, from the oldest to the newest edition.
    """

    def sort_marks(self, marks: List[Mark]) -> List[Mark]:
        return sorted(marks, key=lambda mark: mark.event.edition.year)


class SortStrategyDecorator(SortStrategy, ABC):
    """
    Abstract decorator class to the SortStrategy interface.
    The main goal of this class is to add new features to a given SortStrategy implementation.
    """

    def __init__(self, sort_strategy: SortStrategy):
        self.__sort_strategy = sort_strategy

    @property
    def sort_strategy(self):
        return self.__sort_strategy

    @sort_strategy.setter
    def sort_strategy(self, sort_strategy: SortStrategy):
        self.__sort_strategy = sort_strategy



class AthleteBestMark(SortStrategyDecorator):
    """
    This decorator preserves the ordering given by SortStrategy which it decorates,
    but only keeps one mark for each athlete, which is the first one that appears in the list after applying
    the sort strategy from the SortStrategy object this class decorates.
    """

    def sort_marks(self, marks: List[Mark]) -> List[Mark]:
        # first we need to sort the marks according to the SortStrategy object being decorated
        # and only after we can select the best mark from each athlete.
        marks_after_sorting = self.sort_strategy.sort_marks(marks)
        unique_athlete_marks = self.remove_duplicated_athletes(marks_after_sorting)

        return unique_athlete_marks

    def remove_duplicated_athletes(self, marks: List[Mark], position: int = 1) -> List[Mark]:
        """
        Remove duplicate athletes and store only one mark from each athlete, which is considered to be their best
        result. Since the marks are already ordered at this stage, this method checks if a mark at position N
        has an athlete who already has a mark somewhere in the N - 1 positions. If so, the N mark is deleted.
        """
        # if the position is greater than the size of the list, we have already removed all duplicated athletes
        if position >= len(marks):
            return marks

        # removes repeated athlete
        elif self.is_athlete_mark_present(marks, marks[position].athlete, position):
            marks.remove(marks[position])

        # proceeds to the next mark when the current athlete is not a duplicated at this point.
        else:
            position += 1

        # recursive step
        return self.remove_duplicated_athletes(marks, position)

    @staticmethod
    def is_athlete_mark_present(marks: List[Mark], athlete: Athlete, position: int) -> bool:
        """
        Check the existence of a mark owned by the athlete object.

        return True if the athlete's mark is found, False otherwise.
        """
        for mark in marks[:position]:
            if mark.athlete == athlete:
                return True
        return False
