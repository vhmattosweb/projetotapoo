from src.core_classes.BaseObject import BaseObject
from collections import abc


class JsonCreator:
    """
    Class to convert instantiated objects into a Python dictionary or a list that can be parsed into JSON.
    All the public getter methods will be defined as keys in the dictionary, while the returned values will
    be the corresponding values in the dictionary.

    It uses recursion in case the current instance is also a BaseObject. It converts the latter to a dictionary as well.
    """

    def __new__(cls, current_instance):
        # if it's a BaseObject, it means that we need to convert the attributes to a dict
        if isinstance(current_instance, BaseObject):
            return super().__new__(cls)
        # if it's a sequence, it is a list of BaseObjects
        elif isinstance(current_instance, abc.MutableSequence):
            return [cls(instance).get_json() for instance in current_instance]
        # in case it's neither
        else:
            return current_instance

    def __init__(self, current_instance):
        # dictionary to hold the data
        self.__json = {}
        # loop over all attribute of the object
        for attr in dir(current_instance):
            # forgeting about the internal ones
            if not attr.startswith("_"):
                # value of the current attribute
                current_attr = getattr(current_instance, attr)
                # skipping the cases where the attr was not requested by the database
                if current_attr == f"Attribute '{attr}' not found":
                    continue
                # creating the json
                if isinstance(current_attr, (int, float, str)):
                    self.__json[attr] = current_attr
                # another recursive step in case the attr is a BaseObject
                elif isinstance(current_attr, BaseObject):
                    self.__json[attr] = JsonCreator(current_attr).get_json()

    def get_json(self):
        return self.__json
