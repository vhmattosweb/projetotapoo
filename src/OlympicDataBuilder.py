from src.core_classes.Athlete import Athlete
from src.core_classes.Mark import Mark
from src.core_classes.Location import Location
from src.core_classes.EditionData import EditionData
from src.core_classes.Event import Event
from src.core_classes.Modality import Modality
from src.core_classes.Sport import Sport
from database.src.FrozenJSON import FrozenJSON


class OlympicDataBuilder:
    """
    The OlympicDataBuilder class is responsible for reading a FrozenJSON object and creating all objects to represent
    the data inside the object. The class constructs objects in a way that ensures no duplicate objects are created.
    For example, if there are two entries with marks that are linked to the same athlete or country,
    the two mark objects will reference the same athlete or country object.

    Usage:
    olympic_builder = OlympicBuilder()
    olympic_buider.construct_objects(frozen_json_obj)
    all_results = olympic_builder.get_olympic_data()
    """
    # Dictionary to hold all classes that will be used to create the objects.
    # The key is the name that appears in the FrozenJSON objects,
    # which serves as a flag indicating that an object must be created.
    classes = {
        "athlete": Athlete,
        "mark": Mark,
        "location": Location,
        "event": Event,
        "editiondata": EditionData,
        "modality": Modality,
        "sport": Sport
    }

    def __init__(self):
        # dictionary to store the objects
        self.created_objects = {
            class_name: list() for class_name in self.classes
        }
        # list of records we'll return to the client
        self._records = list()


    def construct_objects(self, json_data: FrozenJSON):
        """Reads the data and instantiate the objects."""
        # loop over all the records
        for record in json_data:
            self._records.append(self.create_object(record))

    def create_object(self, data: FrozenJSON):
        """
        This class creates an object from data using recursion.
        Whenever an object depends on another, the latter is created first.
        """
        # the object has only one key and its value is another dictionary with all its attributes
        class_name = list(data.keys())[0]
        # dict to store all the attrs
        object_attrs = {}
        # loop over the attribute
        for feature_name, features in data[class_name].items():
            # if the attr is a referenced to another class, we need to create this object
            if feature_name in self.classes:
                object_attrs[feature_name] = self.create_object(FrozenJSON({feature_name: features}))
            # otherwise we just add the feature to the attributes dictionary
            else:
                object_attrs[feature_name] = features
        # creating and returning the object
        return self.create_object_from_class(class_name, object_attrs)

    def create_object_from_class(self, class_name: str, attrs: dict):
        """
        Method responsible to create an object from class_name with the attributes defined by attrs.
        If an object of this class with the same attributes already exists, the method returns it instead
        of creating a new one.
        """
        # checking if the object already exists
        for obj_from_class in self.created_objects[class_name]:
            # if the object with these attrs exists, we return it
            if all([getattr(obj_from_class, "_" + attr) == value for attr, value in attrs.items()]):
                return obj_from_class
        # in case it does not exist we created it
        new_object = self.classes[class_name](**attrs)
        # and add it to the list of created objects
        self.created_objects[class_name].append(new_object)
        # return the object
        return new_object

    def get_olympic_data(self):
        """Returns the data objects"""
        return self._records
