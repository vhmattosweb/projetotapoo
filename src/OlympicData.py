from src.OlympicDataBuilder import OlympicDataBuilder
import database.src.SelectQueryHandler as query_handler
from database.src.OlympicDataRequest import OlympicDataRequest
from database.src.MySQL import MySQL
from src.core_classes.Filter import Filter


class OlympicData:
    """
    This class is responsible for providing data to the Ranking objects.
    That is, it will be responsible for creating the OlympicDataRequest object, considering the filter that contains
    the requirements that the selected data must satisfy.
    """

    # dictionary of all table in our database
    tables = {
        "Athlete": {"columns": ["id", "name"], "foreign_keys": {"countryId": "Location"}},
        "Location": {"columns": ["id", "name"], "foreign_keys": {}},
        "Mark": {
            "columns": ["value"],
            "foreign_keys": {"eventId": "Event", "athleteId": "Athlete"},
        },
        "Event": {
            "columns": [],
            "foreign_keys": {"editionId": "EditionData", "modalityID": "Modality"},
        },
        "EditionData": {
            "columns": ["id", "name", "year"],
            "foreign_keys": {"cityId": "Location"},
        },
        "Modality": {"columns": ["id", "name"], "foreign_keys": {"sportID": "Sport"}},
        "Sport": {"columns": ["id", "name"], "foreign_keys": {}},
    }

    # database connection
    olympic_database = MySQL(
        "olympicuser", "tap00lympics", "132.226.240.196", 3306, "tapoo"
    )

    def __init__(self):
        # creating the olympic data builder object
        self.olympic_builder = OlympicDataBuilder()
        # creating the single table creator object
        self.table_creator = SingleTableFactory()
        # all single tables
        self.single_tables = {
            table.table_name: table
            for table in self.table_creator.create_tables_from_dict(self.tables)
        }

    def get_marks(self, user_filter: Filter) -> list:
        """Returns all marks that satisfy the user_filter requirements."""
        # constructing the join table
        join_tables = query_handler.JoinTables(list(self.single_tables.values()))
        # creating the select query with the where clause that contains the specifications
        select_query = query_handler.SelectQuery(
            join_tables, user_filter.generate_where_clause()
        )
        # creating the request
        data_request = OlympicDataRequest(select_query, self.olympic_database)
        data_request.request_data()
        # instatiating the objects
        self.olympic_builder.construct_objects(data_request.get_records())
        # return all marks
        return self.olympic_builder.get_olympic_data()

    def get_genercic_table(self, table_name: str) -> list:
        """Returns data from a single table named table_name."""
        select_query = query_handler.SelectQuery(self.single_tables[table_name])
        data_request = OlympicDataRequest(select_query, self.olympic_database)
        data_request.request_data()
        self.olympic_builder.construct_objects(data_request.get_records())
        return self.olympic_builder.get_olympic_data()

    def get_countries(self) -> list:
        """Returns only the locations that are countries and not cities"""
        # creating the tables
        athlete = query_handler.SingleTable("Athlete", "countryId")
        # creating where clause to choose only locations that are foreign keys of the athlete table
        where_clause = query_handler.ForeignKeyCondition("Location", "id", athlete)
        # select object
        select = query_handler.SelectQuery(self.single_tables["Location"], where_clause)
        # making the request
        data_request = OlympicDataRequest(select, self.olympic_database)
        data_request.request_data()
        # constructing objects
        self.olympic_builder.construct_objects(data_request.get_records())
        # returning the list
        return self.olympic_builder.get_olympic_data()

    def get_modality_by_sport(self, sport_id: int) -> list:
        # constructing the join table
        join_table = query_handler.JoinTables([self.single_tables["Modality"], self.single_tables["Sport"]])
        # where clause to select only modalities of a given sport
        choosing_sport = query_handler.SingleWhereCondition("Sport", "id", [sport_id])
        # constructing the select query
        select = query_handler.SelectQuery(join_table, choosing_sport)
        # requesting data
        data_request = OlympicDataRequest(select, self.olympic_database)
        data_request.request_data()
        # creating the objects
        self.olympic_builder.construct_objects(data_request.get_records())
        # retuns the constructed objects
        return self.olympic_builder.get_olympic_data()


class SingleTableFactory:
    """
    Class responsible for creating SingleTable objects.
    It can be a singleton since one instance can create any type of SingleTable object.
    """

    # variable to store the instance
    instance = None

    def __new__(cls):
        """This is required to ensure we are implementing a Singleton"""
        if cls.instance is None:
            cls.instance = super(SingleTableFactory, cls).__new__(cls)
        return cls.instance

    def create_tables_from_dict(self, tables_dict: dict):
        """
        Create SingleTable objects from a dictionary and return a generator over them.
        The dictionary must follow the following structure:

        tables_dict = {
            'name_of_table': {
                'columns': ['column1', 'column2', ...],
                'foreign_keys': {
                    'foreign_key1_name': 'referenced_table',
                    ...
                }
            },
            ...
        }
        """
        # loop over the tables
        for table_name, columns in tables_dict.items():
            yield self.create_table(
                table_name, *columns["columns"], **columns["foreign_keys"]
            )

    @staticmethod
    def create_table(
        table_name: str, *columns: list, **foreign_keys: dict
    ) -> query_handler.SingleTable:
        """Creates a SingleTable object."""
        return query_handler.SingleTable(table_name, *columns, **foreign_keys)
