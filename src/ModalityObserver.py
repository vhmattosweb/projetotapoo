"""
Class to track the modality that is being asked by the user.
"""


class ModalityObserver:
    """
    This class is responsible to keep track of the modality that it's being asked by the user.
    If the modality has marks with unit of distance, it has a method to notify all the marks.
    The is_distance_unit method returns True if the modality has distance units.

    If is_distance_unit is True, how we present the marks and how we sort the ranking is different from the
    case where we have time units.
    This class is a Singleton, since diferrent objects needs to request this information at different times.
    """

    # attribute to store the instance of the observer
    _modality_observer_instance = None
    # id of the modalities with units in distance and not in time
    _distance_unit_modalities_ids = [
        423, 424, 425, 426, 427, 429, 430, 431, 443, 444, 445, 446, 447, 449, 450, 451, 460, 461, 462, 463, 464, 465,
        470, 471, 474
    ]
    # stores the current modality name
    _current_modality = None

    def __new__(cls):
        """Creating a new method to ensure that we implement a Singleton"""
        # if the singleton has not been created, we create it
        if cls._modality_observer_instance is None:
            cls._modality_observer_instance = super(ModalityObserver, cls).__new__(cls)
        # return the Singleton
        return cls._modality_observer_instance

    @classmethod
    def update_modality(cls, modality_id: str):
        """Saves the current modality id"""
        cls._current_modality = int(modality_id)

    @classmethod
    def is_distance_unit(cls) -> bool:
        """Checks with the current modality has units of distance"""
        return cls._current_modality in cls._distance_unit_modalities_ids
