from abc import ABC, abstractmethod
from typing import List
from src.core_classes.Mark import Mark
from src.OlympicData import OlympicData
from src.core_classes.Filter import Filter
from src.SortStrategy import SortStrategy


class Ranking(ABC):
    """Interface to represent the ranking."""

    def __init__(self, sorting_strategy: SortStrategy):
        # the SortingStrategy tells how we should sort the objects in the ranking
        self.sorting_strategy = sorting_strategy

    @abstractmethod
    def get_marks(self) -> List[Mark]:
        """This method must return a list of Mark objects ordered by their attribute named 'value'."""
        pass

    def update_strategy(self, sorting_strategy: SortStrategy):
        self.sorting_strategy = sorting_strategy


class FilteredRanking(Ranking):
    """
    Class to represent the ranking with filters.
    In order to construct the ranking, the client code must specify the filter object, which contains a list of all the
    specifications on the data read from the database that the ranking must take into account.
    """

    def __init__(self, required_filter: Filter, sorting_strategy: SortStrategy):
        super().__init__(sorting_strategy)
        # list to store all the marks
        self.marks: List[Mark] = list()
        # attribute to stpre the OlympicData object
        self.olympic_data: OlympicData = OlympicData()
        # setting the filter
        self._filter = required_filter

    @property
    def filter(self):
        return self._filter

    @filter.setter
    def filter(self, required_filter: Filter):
        self._filter = required_filter

    def get_marks(self) -> List[Mark]:
        # request the marks from the OlympicData object, specifying the filter that we must include in the request
        if not self.marks:
            self.marks = self.olympic_data.get_marks(self.filter)
        return self.sorting_strategy.sort_marks(self.marks)

    def __getitem__(self, item):
        return self.marks[item]

    def __len__(self):
        return len(self.marks)
