import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib.ticker import AutoMinorLocator
from src.core_classes.Mark import Mark
from typing import List
import numpy as np

rc('text', usetex=True)
# rc('font', **{'family': 'Helvetica', 'sans-serif': ['Verdana']})
plt.rcParams.update({'font.size': 20})
plt.rcParams['font.family'] = 'Helvetica'
plt.rcParams['font.weight'] = 'bold'



class AthleteMarksGraph:

    def __init__(self, marks: List[Mark]):
        self.marks = marks

    def plot(self):
        fig, ax = plt.subplots(figsize=(15, 9))
        y_axis = [mark.absolute_value for mark in self.marks]
        limit = (max(y_axis) - min(y_axis)) * 1.05
        ticks = np.linspace(min(y_axis) - limit, max(y_axis) + limit, 5)
        tickes_labels = [Mark(value=value).value for value in ticks]
        x_axis = [mark.event.edition.name for mark in self.marks]
        plt.yticks(ticks, tickes_labels)
        plt.scatter(x_axis, y_axis, color="#a4161a", s=100)
        plt.ylim(min(y_axis) - limit, max(y_axis) + limit)
        plt.ylabel("Mark",  fontsize=25, labelpad=15)
        plt.xlabel("Edition", fontsize=25, labelpad=15)
        athlete = self.marks[0].athlete.name
        modality = self.marks[0].event.modality.name
        plt.title(f"{athlete.title()}' marks in the {modality} event")

        ax.xaxis.set_minor_locator(AutoMinorLocator())
        ax.yaxis.set_minor_locator(AutoMinorLocator())
        ax.tick_params(axis="both", which="minor", top=False, right=False, length=3)
        ax.tick_params(axis="both", which="major", top=False, right=False, length=7,  pad=7)
        # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)

        plt.show()



