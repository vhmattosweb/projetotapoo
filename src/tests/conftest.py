import pytest
import src.core_classes.Filter as filters
from database.src.TableFactory import SingleTableFactory
from database.src import SelectQueryHandler as query_handler
from src.core_classes.Athlete import Athlete
from src.core_classes.Location import Location
from src.core_classes.Sport import Sport
from src.core_classes.Modality import Modality
from src.core_classes.EditionData import EditionData
from src.core_classes.Event import Event
from src.core_classes.Mark import Mark

# all tables
tables = {
    "Athlete": {"columns": ["id", "name"], "foreign_keys": {"countryId": "Location"}},
    "Location": {"columns": ["id", "name"], "foreign_keys": {}},
    "Mark": {
        "columns": ["value"],
        "foreign_keys": {"eventId": "Event", "athleteId": "Athlete"},
    },
    "Event": {
        "columns": [],
        "foreign_keys": {"editionId": "EditionData", "modalityID": "Modality"},
    },
    "EditionData": {
        "columns": ["id", "name", "year"],
        "foreign_keys": {"cityId": "Location"},
    },
    "Modality": {"columns": ["id", "name"], "foreign_keys": {"sportID": "Sport"}},
    "Sport": {"columns": ["id", "name"], "foreign_keys": {}},
}


@pytest.fixture
def select_marks_query():
    single_table_factory = SingleTableFactory()
    # list with all tables and return the composite query
    all_tables = [table for table in single_table_factory.create_tables_from_dict(tables)]
    join_tables = query_handler.JoinTables(all_tables)
    _ = join_tables.get_select_query()
    return join_tables


@pytest.fixture
def edition_filter():
    # ids of the Tokyo 2020, Rio 2016, London 2012
    return filters.EditionFilter(editions_ids=[134, 135, 136])


@pytest.fixture
def sport_filter():
    # athletics id
    return filters.SportFilter(sport_id=22)


@pytest.fixture
def modality_filter():
    # men's 10000m id
    return filters.ModalityFilter(modality_id=411)


@pytest.fixture
def composite_filter(edition_filter, sport_filter):
    return filters.CompositeFilter([edition_filter, sport_filter])


@pytest.fixture
def brasil():
    return Location(name="BRA")


@pytest.fixture
def cesar_cielo(brasil):
    return Athlete(name="Cesar Cielo", location=brasil)


@pytest.fixture
def swimming():
    return Sport(name="Swimming")


@pytest.fixture
def men_100m_free(swimming):
    return Modality(name="Men's 100m freestyle", sport=swimming)


@pytest.fixture
def tokyo2020():
    return EditionData(name="Tokyo 2020")


@pytest.fixture
def event(men_100m_free, tokyo2020):
    return Event(modality=men_100m_free, editiondata=tokyo2020)


@pytest.fixture
def mark(cesar_cielo, event):
    return Mark(athlete=cesar_cielo, event=event, value=47)
