import pytest
from src.core_classes.Modality import Modality


class TestModality:

    @pytest.fixture
    def modality(self):
        return Modality(id=1, name="100m livres", sport="Natação")

    def test_get_name(self, modality):
        assert modality.name == "100m livres"

    def test_set_name(self, modality):
        modality.name = "100m rasos"
        assert modality.name == "100m rasos"

    def test_get_sport(self, modality):
        assert modality.sport == "Natação"

    def test_set_sport(self, modality):
        modality.sport = "Tênis"
        assert modality.sport == "Tênis"
