import pytest
from src.core_classes.Sport import Sport


class TestSport:

    @pytest.fixture
    def sport(self):
        return Sport(id=1, name="Natação")

    def test_get_name(self, sport):
        assert sport.name == "Natação"

    def test_set_name(self, sport):
        sport.name = "Tênis"
        assert sport.name == "Tênis"
