import pytest
from src.core_classes.Location import Location

class TestLocation:

    @pytest.fixture
    def location(self):
        return Location(id=1, name="Brasil")

    def test_get_name(self, location):
        assert location.name == "Brasil"

    def test_set_name(self, location):
        location.name = "Argentina"
        assert location.name == "Argentina"
