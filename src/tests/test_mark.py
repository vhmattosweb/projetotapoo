import pytest
from src.core_classes.Mark import Mark

class TestMark:

    @pytest.fixture
    def mark(self):
        return Mark(id=1, value=0.01, athlete="César Cielo", event="Paris 2024")

    def test_get_value(self, mark):
        assert mark.value == 0.01

    def test_set_value(self, mark):
        mark.value = 0.02
        assert mark.value == 0.02

    def test_get_athlete(self, mark):
        assert mark.athlete == "César Cielo"

    def test_set_athlete(self, mark):
        mark.athlete = "César Filho"
        assert mark.athlete == "César Filho"

    def test_get_event(self, mark):
        assert mark.event == "Paris 2024"

    def test_set_event(self, mark):
        mark.event = "Rio 2016"
        assert mark.event == "Rio 2016"

    def test_lt(self, mark):
        other_mark = Mark(id=1, value=0.08, athlete="César Cielo", event="Paris 2024")
        assert mark < other_mark
