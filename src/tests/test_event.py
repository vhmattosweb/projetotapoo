import pytest
from src.core_classes.Event import Event
from src.core_classes.Modality import Modality
from src.core_classes.Sport import Sport


class TestEvent:

    @pytest.fixture
    def sport(self):
        return Sport(id=1, name="Natação")

    @pytest.fixture
    def modality(self, sport):
        return Modality(id=2, name="100m livres", sport=sport)

    @pytest.fixture
    def event(self, modality):
        return Event(id=1, modality=modality, editiondata="Paris 2024")

    def test_get_modality(self, event, modality):
        assert event.modality == modality

    def test_get_edition(self, event):
        assert event.edition == "Paris 2024"

