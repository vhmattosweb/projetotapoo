import pytest
from database.src.FrozenJSON import FrozenJSON
from src.OlympicDataBuilder import OlympicDataBuilder


class TestOlympicDataBuilder:
    """Test if the OlympicDataBuilder is able to create objects that implements the BaseObject class"""

    def setup_method(self):
        self.olympic_data_builder = OlympicDataBuilder()

    def base_object_factory(self, features_dictionary):
        # creating a BaseObject from a FrozenJson object
        frozen_json = FrozenJSON(features_dictionary)
        base_object = self.olympic_data_builder.create_object(frozen_json)
        return base_object

    def test_brasil_location(self, brasil):
        location_dict = {
            "location": {
                "name": "BRA"
            }
        }
        # creating the object from a frozen json object
        location_obj = self.base_object_factory(location_dict)
        assert location_obj.name == brasil.name

    def test_cesar_cielo(self, cesar_cielo):
        athlete_dictionary = {
            "athlete": {
                "name": "Cesar Cielo",
                "location": {
                    "name": "BRA"
                }
            }
        }
        # creating the athlete object from a frozenjson object
        athlete_obj = self.base_object_factory(athlete_dictionary)
        # comparing both objects
        assert athlete_obj.name == cesar_cielo.name and athlete_obj.country.name == cesar_cielo.country.name

    def test_sport(self, swimming):
        swimming_dict = {
            "sport": {"name": "Swimming"}
        }
        # creating the sport object from a frozenjson object
        sport_obj = self.base_object_factory(swimming_dict)
        # comparing both objects
        assert sport_obj.name == swimming.name

    def test_modality(self, men_100m_free):
        modality_dict = {
            "modality": {
                "name": "Men's 100m freestyle",
                "sport": {"name": "Swimming"}
            }
        }
        modality_obj = self.base_object_factory(modality_dict)
        assert modality_obj.name == men_100m_free.name and modality_obj.sport.name == men_100m_free.sport.name

    def test_edition(self, tokyo2020):
        edition_dict = {
            "editiondata": {"name": "Tokyo 2020"}
        }
        editiondata_obj = self.base_object_factory(edition_dict)
        assert editiondata_obj.name == tokyo2020.name

    def test_event(self, event):
        event_dict = {
            "event": {
                "modality": {
                    "name": "Men's 100m freestyle",
                    "sport": {"name": "Swimming"}
                },
                "editiondata": {"name": "Tokyo 2020"}
            }
        }
        event_obj = self.base_object_factory(event_dict)
        assert event_obj.modality.name == event.modality.name and event_obj.edition.name == event.edition.name

    def test_mark(self, mark):
        mark_dict = {
            "mark": {
                "athlete": {
                    "name": "Cesar Cielo",
                    "location": {
                        "name": "BRA"
                    }
                },
                "event": {
                    "modality": {
                        "name": "Men's 100m freestyle",
                        "sport": {"name": "Swimming"}
                    },
                    "editiondata": {"name": "Tokyo 2020"}
                },
                "value": 47
            }
        }
        mark_obj = self.base_object_factory(mark_dict)
        assert (mark_obj.athlete.name == mark.athlete.name and
                mark_obj.event.modality.name == mark.event.modality.name and
                mark_obj.value == mark.value and
                mark_obj.event.edition.name == mark.event.edition.name)
