import pytest
from src.JsonCreator import JsonCreator


class TestJsonCreator:

    def test_athlete_creator(self, cesar_cielo):
        athlete_dictionary = {
            "name": "Cesar Cielo",
            "country": {
                "name": "BRA"
            }
        }
        assert JsonCreator(cesar_cielo).get_json() == athlete_dictionary

    def test_location_creator(self, brasil):
        location_dict = {"name": "BRA"}
        assert JsonCreator(brasil).get_json() == location_dict

    def test_sport_creator(self, swimming):
        sport_dict = {"name": "Swimming"}
        assert JsonCreator(swimming).get_json() == sport_dict

    def test_modality_creator(self, men_100m_free):
        modality_dict = {"name": "Men's 100m Freestyle's", "sport": {"name": "Swimming"}}
        assert JsonCreator(men_100m_free).get_json() == modality_dict

    def test_edition_creator(self, tokyo2020):
        edition_dict = {"name": "Tokyo 2020"}
        assert JsonCreator(tokyo2020).get_json() == edition_dict

    def test_event_creator(self, event):
        event_dict = {
            "modality": {
                "name": "Men's 100m Freestyle's",
                "sport": {"name": "Swimming"}
            },
            "edition": {"name": "Tokyo 2020"}
        }
        assert JsonCreator(event).get_json() == event_dict

    def test_mark_creator(self, mark):
        mark_dict = {
            "absolute_value": 47,
            "value": "47.00",
            "athlete": {
                "name": "Cesar Cielo",
                "country": {
                    "name": "BRA"
                }
            },
            "event": {
                "modality": {
                    "name": "Men's 100m Freestyle's",
                    "sport": {"name": "Swimming"}
                },
                "edition": {"name": "Tokyo 2020"}
            }
        }
        assert JsonCreator(mark).get_json() == mark_dict
