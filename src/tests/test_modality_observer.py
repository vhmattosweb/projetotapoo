import pytest
from src.ModalityObserver import ModalityObserver


class TestModalityObserver:

    @pytest.mark.parametrize("modality_id", "423 424 425 426 427 429 430 431".split())
    def test_distance_unit_modality(self, modality_id):
        modality_observer = ModalityObserver()
        modality_observer.update_modality(modality_id)
        assert modality_observer.is_distance_unit() == True

    @pytest.mark.parametrize("modality_id", "383 384 386 388 389 400".split())
    def test_time_unit_modality(self, modality_id):
        modality_observer = ModalityObserver()
        modality_observer.update_modality(modality_id)
        assert modality_observer.is_distance_unit() == False
