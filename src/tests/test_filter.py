import pytest
import itertools


class TestFilter:

    def test_edition_filter(self, edition_filter, select_marks_query):
        expected_result = "WHERE Event_EditionData.id IN ('134', '135', '136')"
        assert edition_filter.generate_where_clause().get_where_clause(select_marks_query) == expected_result

    def test_sport_filter(self, sport_filter, select_marks_query):
        expected_result = "WHERE Modality_Sport.id IN ('22')"
        assert sport_filter.generate_where_clause().get_where_clause(select_marks_query) == expected_result

    def test_modality_filter(self, modality_filter, select_marks_query):
        expected_result = "WHERE Event_Modality.id IN ('411')"
        assert modality_filter.generate_where_clause().get_where_clause(select_marks_query) == expected_result

    def test_add_filter(self, composite_filter, modality_filter, select_marks_query):
        composite_filter.add_filter(modality_filter)
        conditions = [
            "Event_EditionData.id IN ('134', '135', '136')", "Event_Modality.id IN ('411')",
            "Modality_Sport.id IN ('22')"
        ]
        # the order in which the conditions appear shouldn't matter
        possible_cases = [f"WHERE {' AND '.join(result)}" for result in itertools.permutations(conditions, 3)]
        assert composite_filter.generate_where_clause().get_where_clause(select_marks_query) in possible_cases
