import pytest
from src.OlympicData import OlympicData
from src.core_classes.Filter import (
    SportFilter,
    ModalityFilter,
    CountryFilter,
    CompositeFilter,
)


class TestOlympicData:
    def setup_method(self):
        self.olympic_data = OlympicData()

    def test_get_marks(self):
        sport_id = 22
        modality_id = 411
        country_ids = [409, 427]

        composite_filter = CompositeFilter(
            [
                SportFilter(sport_id),
                ModalityFilter(modality_id),
                CountryFilter(country_ids),
            ]
        )
        marks = self.olympic_data.get_marks(composite_filter)

        assert isinstance(marks, list)
        assert len(marks) > 0

        for mark in marks:
            assert mark.event.modality.sport.id == sport_id
            assert mark.event.modality.id == modality_id
            assert mark.athlete.country.id in country_ids

    def test_get_generic_table(self):
        sports = self.olympic_data.get_genercic_table("Sport")
        assert isinstance(sports, list)
        assert len(sports) == 2

        for data in sports:
            assert data.name.lower() in ["swimming", "athletics"]

    def test_get_countries(self):
        countries = self.olympic_data.get_countries()
        assert isinstance(countries, list)
        assert len(countries) > 0

        country_names = [c.name.upper() for c in countries]
        assert "TOKYO" not in country_names
        assert "JPN" in country_names

        assert "RIO" not in country_names
        assert "BRA" in country_names

        assert "LONDON" not in country_names
        assert "GBR" in country_names


    def test_get_modality_by_sport(self):
        sport_id = 23
        modalities = self.olympic_data.get_modality_by_sport(sport_id)
        assert isinstance(modalities, list)
        assert len(modalities) > 0
        assert all([m.sport.id == sport_id for m in modalities])
