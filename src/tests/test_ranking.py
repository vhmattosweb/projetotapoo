import pytest
from src import Rankings as rankings
from src import SortStrategy as strategy
from src.core_classes import Filter as filters
import json


class TestFilteredRanking:
    """
    Checks the ranking again a few known benchmark results.
    """

    @staticmethod
    def get_results_from(edition_id, modality_id):
        # creating the filters
        edition_filter = filters.EditionFilter([edition_id])
        modality_filter = filters.ModalityFilter(modality_id)
        all_filters = filters.CompositeFilter([edition_filter, modality_filter])
        # creating the filtered ranking
        return rankings.FilteredRanking(all_filters, strategy.SortByBestMark())

    @staticmethod
    def read_expected_results_from_json(filename):
        file_ = open(filename)
        expected_result = json.load(file_)
        return expected_result

    @staticmethod
    def transform_to_dict(ranking):
        # marks from the ranking
        marks = ranking.get_marks()
        # storing the marks into a list of dicts to compare with the expected result
        marks_dict = []
        for mark in marks:
            marks_dict.append({
                "athlete": mark.athlete.name,
                "country": mark.athlete.country.name,
                "mark": mark.value
            })
        return marks_dict

    def test_100m_male_free_tokyo2020(self):
        # Results from 100m male freestyle from Tokyo 2020
        ranking = self.get_results_from(134, 398)
        result_from_rank = self.transform_to_dict(ranking)
        # expected result
        expected = self.read_expected_results_from_json("rankings/100m_male_free_Tokyo2020.json")
        assert result_from_rank == expected

    def test_100m_male_rio2016(self):
        # results from 100m male from Rio 2016
        ranking = self.get_results_from(135, 412)
        result_from_rank = self.transform_to_dict(ranking)
        # expected result
        expected = self.read_expected_results_from_json("rankings/100m_rio2016.json")
        assert result_from_rank == expected

    def test_200m_medley_male_london2012(self):
        # results from 100m male from Rio 2016
        ranking = self.get_results_from(136, 409)
        result_from_rank = self.transform_to_dict(ranking)
        # expected result
        expected = self.read_expected_results_from_json("rankings/200m_individual_medley_london2012.json")
        assert result_from_rank == expected


class TestRankByAthletes:
    """Tests with the algorithm in RankByAthlete is working properly"""

    @pytest.fixture
    def simple_ranking(self):
        """Creates the ranking of marks for the 50m freestyle women"""
        # creating the 50m free women and constructing the ranking
        modality_filter = filters.ModalityFilter(383)
        return rankings.FilteredRanking(modality_filter, strategy.SortByBestMark())

    @staticmethod
    def select_best_marks(ranking):
        """Keeps only the best mark of a given athlete"""
        # marks
        marks = ranking.get_marks()
        # initial index
        index = 0
        # removing the duplicated marks
        while index < len(marks):
            athlete_name = marks[index].athlete.name
            remove_mark = False
            # checking if the athlete name has already appeared
            for mark in marks[:index]:
                if mark.athlete.name == athlete_name:
                    remove_mark = True
                    break
            if remove_mark:
                marks.remove(marks[index])
            else:
                index += 1
        return marks


    def test_athletes_ranking(self, simple_ranking):
        best_athlete_mark_strategy = strategy.AthleteBestMark(strategy.SortByBestMark())
        simple_ranking.update_strategy(best_athlete_mark_strategy)
        ranking_list = simple_ranking.get_marks()
        expected_ranking = self.select_best_marks(simple_ranking)
        for result, expected in zip(ranking_list[:10], expected_ranking[:10]):
            assert result == expected


