import pytest
from src.core_classes.Athlete import Athlete
from src.core_classes.Location import Location


class TestAthlete:

    @pytest.fixture
    def country(self):
        return Location(id=2, name="Brasil")

    @pytest.fixture
    def athlete(self, country):
        return Athlete(id=1, name="César Cielo", location=country)

    def test_get_name(self, athlete):
        assert athlete.name == "César Cielo"

    def test_get_country(self, athlete, country):
        assert athlete.country == country

    def test_is_from(self, athlete, country):
        assert athlete.is_from(country=country) == True
