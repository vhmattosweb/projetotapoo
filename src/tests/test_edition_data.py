import pytest
from src.core_classes.EditionData import EditionData


class TestEditionData:

    @pytest.fixture
    def edition_data(self):
        return EditionData(id=1, name="Rio 2016", city="Rio de Janeiro", year=2016)

    def test_get_name(self, edition_data):
        assert edition_data.name == "Rio 2016"

    def test_set_name(self, edition_data):
        edition_data.name = "Paris 2024"
        assert edition_data.name == "Paris 2024"

    def test_get_city(self, edition_data):
        assert edition_data.city == "Rio de Janeiro"

    def test_set_city(self, edition_data):
        edition_data.city = "Paris"
        assert edition_data.city == "Paris"

    def test_get_year(self, edition_data):
        assert edition_data.year == 2016

    def test_set_year(self, edition_data):
        edition_data.year = 2024
        assert edition_data.year == 2024
