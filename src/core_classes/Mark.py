from src.core_classes.BaseObject import BaseObject
from src.ModalityObserver import ModalityObserver


class Mark(BaseObject):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # storing the modality observer object
        self.__modality_observer = ModalityObserver()

    @property
    @BaseObject.handle_exception
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    @BaseObject.handle_exception
    def value(self):
        # checks if the current modality has units of time of distance
        if not self.__modality_observer.is_distance_unit():
            return self.correct_mark()
        return f"{self._value:.2f}"

    @value.setter
    def value(self, val):
        self._value = val

    @property
    @BaseObject.handle_exception
    def absolute_value(self):
        return self._value

    @property
    @BaseObject.handle_exception
    def athlete(self):
        return self._athlete

    @athlete.setter
    def athlete(self, val):
        self._athlete = val

    @property
    @BaseObject.handle_exception
    def event(self):
        return self._event

    @event.setter
    def event(self, event):
        self._event = event

    def __repr__(self):
        return f"{self.athlete!r} - {self.value!r}, {self.event!r}"

    def __lt__(self, other):
        return self.absolute_value < other.absolute_value

    def correct_mark(self):
        """
        All marks in the database are in seconds, but to display them, we might need to convert it to minutes
        or hours. This method handles this convertion.
        """
        # 1 min = 60s, 1 hora = 60 min
        minutes, seconds = divmod(self.absolute_value, 60)
        hours, minutes = divmod(minutes, 60)
        # fixing the string to present in the ranking
        final_str = f"{seconds:05.2f}" if hours == 0 else f"{seconds:04.1f}"
        for part in (minutes, hours):
            if part > 0:
                final_str = f"{part:02.0f}:" + final_str if part == minutes else f"{part:01.0f}:" + final_str

        return final_str

