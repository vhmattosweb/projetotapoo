from src.core_classes.Modality import Modality
from src.core_classes.BaseObject import BaseObject


class Event(BaseObject):

    def is_modality(self, modality: Modality) -> bool:
        return self._modality == modality

    @property
    @BaseObject.handle_exception
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    @BaseObject.handle_exception
    def modality(self):
        return self._modality

    @modality.setter
    def modality(self, value):
        self._modality = value

    @property
    @BaseObject.handle_exception
    def edition(self):
        return self._editiondata

    @edition.setter
    def edition(self, edition):
        self._editiondata = edition


    def __repr__(self):
        return f"{self.modality!r} - {self.edition!r}"
