from abc import ABC, abstractmethod
from typing import List
from database.src.SelectQueryHandler import (WhereClause, ManyWhereCondition, SingleWhereCondition)
from src.ModalityObserver import ModalityObserver


class Filter(ABC):
    """
    This interface must generate a WhereClause object containing the user-selected filters for retrieving data
    from the database, such as specifying the country, edition, modality, and so on.
    """
    @abstractmethod
    def generate_where_clause(self) -> WhereClause:
        """
        Generates a WhereClause object with the requirements that the selected data from the database must fulfill.
        """
        pass


class CompositeFilter(Filter):
    """
    The CompositeFilter class receives a list of SingleFilters and generates a WhereClause
    that is a composition of the requirements from all filters.
    """

    def __init__(self, filters: List[Filter]):
        self.filters: List[Filter] = filters

    def add_filter(self, new_filter: Filter):
        """Add new filter to the list of filters"""
        self.filters.append(new_filter)

    def generate_where_clause(self) -> WhereClause:
        """Returns the WhereClause object, taking into account all filter specifications"""
        return ManyWhereCondition([filter_.generate_where_clause() for filter_ in self.filters])


class SingleFilter(Filter):
    """Class to represent an individual requirement set by the user."""

    def __init__(self, table_name: str, column_name: str, target_values: list):
        """
        To construct the WhereClause object, we must have the following parameters.

        :param table_name: the name of the table in the database.
        :param column_name: the name of the column for which we need to specify the values.
        :param target_values: the values that 'colum_name' can have to select the data.
        """
        self.table_name: str = table_name
        self.target_values: list = target_values
        self.column_name: str = column_name

    def add_target_value(self, target_value: str):
        """If we need to include more values in the 'target_values' list..."""
        self.target_values.append(target_value)

    def generate_where_clause(self) -> WhereClause:
        return SingleWhereCondition(self.table_name, self.column_name, self.target_values)


class EditionFilter(SingleFilter):
    """A filter to specify the editions we need to take into account when constructing the ranking."""
    def __init__(self, editions_ids: List[int]):
        super().__init__("EditionData", "id", editions_ids)


class CountryFilter(SingleFilter):
    """A filter to specify the countries from which the athletes must be to construct the ranking."""
    def __init__(self, country_ids: List[int]):
        super().__init__("Location", "id", country_ids)


class AthleteFilter(SingleFilter):
    """A filter to specify the athletes that we must consider to construct the ranking."""
    def __init__(self, athletes: List[int]):
        super().__init__("Athlete", "id", athletes)


class ModalityFilter(SingleFilter):
    """A filter to specify the modality we must consider to construct the ranking."""
    def __init__(self, modality_id):
        # creating the modality observer object
        modality_obs = ModalityObserver()
        # saving the current modality id
        modality_obs.update_modality(modality_id)
        # creating the table
        super().__init__("Modality", "id", [modality_id])


class SportFilter(SingleFilter):
    """A filter to specify the sport we must consider to construct the ranking."""
    def __init__(self, sport_id: int):
        super().__init__("Sport", "id", [sport_id])
