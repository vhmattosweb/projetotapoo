class BaseObject:
    def __init__(self, **kwargs):
        for attr_name, value in kwargs.items():
            setattr(self, "_" + attr_name, value)

    @staticmethod
    def handle_exception(getter):
        def exception(self):
            try:
                return getter(self)
            except AttributeError:
                return f"Attribute '{getter.__name__}' not found"
        return exception
