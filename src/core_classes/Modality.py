from __future__ import annotations
from src.core_classes.BaseObject import BaseObject
import string


class Modality(BaseObject):

    def __str__(self) -> str:
        return self.name

    @property
    @BaseObject.handle_exception
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    @BaseObject.handle_exception
    def name(self):
        # better way to present the name
        names = [string.capwords(word) for word in self._name.split("_")]
        # transforming singular to plural
        names[0] += "'s"
        # return name
        return " ".join(names)

    @name.setter
    def name(self, value):
        self._name = value

    @property
    @BaseObject.handle_exception
    def sport(self):
        return self._sport

    @sport.setter
    def sport(self, value):
        self._sport = value

    def __repr__(self):
        return f"{self.name!r}"

    def __lt__(self, other: Modality):
        return self.name < other.name
