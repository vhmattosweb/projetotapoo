from src.core_classes.Location import Location
from src.core_classes.BaseObject import BaseObject


class Athlete(BaseObject):

    @property
    @BaseObject.handle_exception
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    @BaseObject.handle_exception
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    @BaseObject.handle_exception
    def country(self):
        return self._location

    @country.setter
    def country(self, country: Location):
        self._location = country

    def is_from(self, country: Location) -> bool:
        return self.country == country

    def __repr__(self):
        return f'{self.name!r} ({self.country!r})'
