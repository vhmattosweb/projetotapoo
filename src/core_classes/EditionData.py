from __future__ import annotations
from src.core_classes.BaseObject import BaseObject


class EditionData(BaseObject):

    @property
    @BaseObject.handle_exception
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    @BaseObject.handle_exception
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    @BaseObject.handle_exception
    def city(self):
        return self._city

    @city.setter
    def city(self, value):
        self._city = value

    @property
    @BaseObject.handle_exception
    def year(self):
        return self._year

    @year.setter
    def year(self, value):
        self._year = value

    def __repr__(self):
        return f"{self.name!r}"

    def __lt__(self, other: EditionData):
        return self.year < other.year

