from __future__ import annotations
from src.core_classes.BaseObject import BaseObject


class Location(BaseObject):

    @property
    @BaseObject.handle_exception
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    @BaseObject.handle_exception
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    def __repr__(self):
        return f"{self.name!r}"

    def __lt__(self, other: Location):
        return self.name < other.name

