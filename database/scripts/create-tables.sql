CREATE DATABASE IF NOT EXISTS tapoo;
USE tapoo;

CREATE TABLE Location (
    id int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(200)
);

CREATE TABLE EditionData (
    id int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(200),
    year int,
    cityId int,
    FOREIGN KEY (cityId) REFERENCES Location(id)
);

CREATE TABLE Sport (
    id int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(200)
);

CREATE TABLE Modality (
    id int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(200),
    sportID int,
    FOREIGN KEY (sportID) REFERENCES Sport(id)
);

CREATE TABLE Event (
    id int PRIMARY KEY AUTO_INCREMENT,
    modalityID int,
    editionId int,
    FOREIGN KEY (editionId) REFERENCES EditionData(id),
    FOREIGN KEY (modalityID) REFERENCES Modality(id)
);

CREATE TABLE Athlete (
    id int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(200),
    countryId int,
    FOREIGN KEY (countryId) REFERENCES Location(id)
);

CREATE TABLE Mark (
    id int PRIMARY KEY AUTO_INCREMENT,
    value FLOAT,
    eventId int,
    athleteId int,
    FOREIGN KEY (eventId) REFERENCES Event(id),
    FOREIGN KEY (athleteId) REFERENCES Athlete(id)
);

