import re
import numpy as np
from database.src.FrozenJSON import FrozenJSON
from database.src.OlympicDataInserter import OlympicDataInserter as odi



class Insertion:
    """Class to handle the insertion into the database"""

    forbitten_modalities = [
        "men_decathlon", "women_heptathlon", "women_pentathlon", "men_pentathlon", "men_freestyle_4x100m",
        "women_freestyle_4x100m"
    ]

    def __init__(self, frozen_json: FrozenJSON, insertor: odi):
        # frozen json file
        self.__json: FrozenJSON = frozen_json
        self.__dbase: odi = insertor

    def run_insertion(self):
        """Initialize the insertion into the database"""
        # inserting the two sports we are interested in
        athletics_id = self.__dbase.insert_sport("Athletics")
        swimming_id = self.__dbase.insert_sport("Swimming")
        # loop over the editions
        for edition, results in self.__json:
            # insert edition
            edition_id = self.__dbase.insert_edition(*self.edition_info(edition))
            print("INFO: Inserting data from edition %s" % edition)
            # insert results from athletics
            self.insert_events_from_sport(athletics_id, edition_id, results.athletics)
            # insert results from swimming
            self.insert_events_from_sport(swimming_id, edition_id, results.swimming)

    def insert_events_from_sport(self, sport_id: int, edition_id: int, events: FrozenJSON):
        """Inserts all events from a specific sport"""
        # loop over all the events
        for modality, event_results in events:
            # modalities we won't take into account in our ranking
            if modality in self.forbitten_modalities or "relay" in modality or "team" in modality:
                continue
            print("INFO: Inserting marks from %s" % modality)
            # getting modality id
            modality_id = self.__dbase.insert_modality(modality, sport_id)
            # inserting the results from the event
            self.insert_event_data(modality_id, edition_id, event_results)


    def insert_event_data(self, modality_id: int, edition_id: int, event_data: FrozenJSON):
        """Inserts all information about one event"""
        # inserting event
        event_id = self.__dbase.insert_event(modality_id, edition_id)
        # loop over the marks
        for mark in event_data:
            # checking if it's a viable mark
            if self.is_viable_result(str(mark.result)):
                # getting the athlete id
                athlete_name = mark.athlete.replace("'", "\\'")
                athlete_id = self.__dbase.insert_athlete(athlete_name, mark.country)
                # convert the resuts to seg.milsegunds if necessary
                result = self.adjust_result(mark.result)
                # inserting mark
                _ = self.__dbase.insert_mark(result, event_id, athlete_id)

    @staticmethod
    def edition_info(edition_name: str) -> tuple:
        """Returns all the info concerning one edition. Name, year, and city name"""
        *city_name, year = edition_name.split("_")
        city_name = " ".join([name.capitalize() for name in city_name])
        # edition name
        edition_name = city_name + " " + year
        # returns all the needed fields
        return edition_name, year, city_name

    @staticmethod
    def is_viable_result(result: str):
        """Checks if the mark is a viable one"""
        if bool(re.search(r'[a-zA-Z]', result)) or result == "" or "-" in result or "/" in result:
            return False
        return True

    @staticmethod
    def adjust_result(mark: str):
        """
        Transform the result to floats.
        If the data is in the form hour:min:seg we transform it to seconds
        """
        results = mark.split(":")
        # converting min and hours to seconds
        results_secs = np.sum([
            np.power(60, i) * float(result) for i, result in zip(range(len(results)), results[::-1])
        ])
        # retuning results
        return np.round(results_secs, 2)
