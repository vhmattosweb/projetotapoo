from database.src.SelectQueryHandler import SelectQuery
from database.src.Database import Database
from database.src.FrozenJSON import FrozenJSON


class OlympicDataRequest:
    """
    Class responsible to access the database and select the data specified by the SelectQuery object.
    It returns a FrozenJSON object with all the information collected from database.
    """

    def __init__(self, select_query: SelectQuery, database: Database):
        self.__select_query: SelectQuery = select_query
        self.__database: Database = database
        # list to store all the records from the request
        self.__records: list = list()
        self.__columns_labels = None

    def request_data(self):
        """Request the data from the database"""
        # connection with database
        self.__database.connect()
        # get the query from the select_query object
        sql_query = self.__select_query.get_select_query()
        # get the columns labels (this way we can know what each column represent)
        self.__columns_labels = self.__select_query.get_columns_labels()
        # loop over the results of the search
        for record in self.__database.execute_query(sql_query):
            self.store_record(record)
        # end connection with database
        self.__database.end_connection()


    def store_record(self, record: tuple):
        """Creates a dictionary with all the information that is in record and stores it into the records list."""
        record_dict = {}
        # loops over all the entries in record
        for result, column in zip(record, self.__columns_labels):
            self.create_attribute(column.split("."), result, record_dict)
        self.__records.append(record_dict)


    def create_attribute(self, column: list, result, record_dict: dict):
        """
        Creates the dictionary to store the record recursively.
        Example:
            column = Mark.Athlete.name, result = Matheus, the record_dict must hav the following entry
            record_dict = {
                'mark': {
                    'athlete': {
                        'name': 'Matheus', ...
                    }, ...
                }
        """
        attr, *remaining = column
        # if remaining does not have size zero it means that we need to create more dictionaries to encapsulate
        # the result.
        if len(remaining):
            # if the attr is not already in the dictionary we must create it
            if attr not in record_dict:
                record_dict[attr] = {}
            # recursive step: constructing the dict into the key we created it.
            self.create_attribute(remaining, result, record_dict[attr])
        # if it's not the case, we just set the result
        else:
            record_dict[attr] = result

    def get_records(self) -> FrozenJSON:
        """Returns a FrozenJSON object constructed out of the records list."""
        return FrozenJSON(self.__records)
