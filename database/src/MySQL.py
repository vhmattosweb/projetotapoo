import mysql.connector
from mysql.connector import errorcode
from database.src.Database import Database


class MySQL(Database):

    def __init__(self, usr: str, pwd: str, host: str, port: int, db: str):
        """
        Constructor.

        :param usr: user
        :param pwd: password
        :param host: host name
        :param port: port number
        :param db: database name
        """
        self.usr = usr
        self.pwd = pwd
        self.host = host
        self.port = port
        self.db = db
        # variable to store the coneection with the database
        self.connection = None

    def connect(self) -> bool:
        try:
            self.connection = mysql.connector.connect(
                user=self.usr, password=self.pwd, host=self.host, port=self.port, database=self.db
            )
            return True
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
            return False

    def end_connection(self) -> bool:
        """Ends the connection with the database"""
        try:
            self.connection.close()
            return True
        except Exception as e:
            print(f"There as an error closing your connection: {e}")
            return False

    def execute_query(self, query: str, insertion_query: bool = False) -> list:
        cursor = self.connection.cursor()
        cursor.execute(query)
        elems = [elem for elem in cursor]
        if insertion_query:
            self.connection.commit()
        cursor.close()
        return elems

    def select(self, table: str, columns: list, where_clause: str = "") -> list:

        query = f"SELECT {', '.join(columns)} FROM {table}"

        if len(where_clause) > 0:
            query += f" WHERE {where_clause}"

        return self.execute_query(query)

    def insert(self, table: str, columns: list, values: list) -> None:

        query = f"INSERT INTO {table} ({', '.join(columns)}) \
                  VALUES ({', '.join(values)})"

        self.execute_query(query, True)

    def update(self, table: str, values: list, where_clause: str) -> bool:

        query = f"UPDATE {table} \
                  SET {', '.join(values)} WHERE {where_clause}"

        self.execute_query(query, True)

    def delete(self, table: str, where_clause: str) -> bool:

        query = f"DELETE FROM {table} \
                     WHERE {where_clause}"

        self.execute_query(query, True)
