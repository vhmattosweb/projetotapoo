from database.src.Database import Database
from database.src.OlympicDataInserter import OlympicDataInserter


class OlympicDataInserterAdapter(OlympicDataInserter):
    """Adapter class to insert data into the dabase"""

    def __init__(self, database: Database):
        self.database = database
        # initializing the connection
        if self.database.connect():
            print("INFO: Connection with database achived.")

    def is_already_in_database(self, table: str, **kwargs) -> bool:
        """Checks wheter value is already in table"""
        # creating the where_clause
        conditions = [f"{key}='{value}'" for key, value in kwargs.items()]
        where_clause = "".join([condition if index == len(conditions) - 1 else condition + " AND "
                                for condition, index in zip(conditions, range(len(conditions)))])
        # reading the rows
        row = self.database.select(table, ["*"], where_clause)
        # returns True if there is a row with these specifications
        return False if len(row) == 0 else True

    def insert_edition(self, name: str, year: str, city_name: str) -> int:
        """Inserts edition into database and returns the ID of the inserted edition"""
        if not self.is_already_in_database("EditionData", name=name):
            # insert the city
            city_id = self.insert_location(city_name)
            # insert edition
            self.database.insert(
                "EditionData",
                ["name", "year", "cityId"],
                [f"'{name}'", f"'{year}'", f"'{city_id}'"],
            )
        # returns inserted id
        return self.database.select("EditionData", ["id"], f"name = '{name}'")[0][0]

    def insert_sport(self, name: str) -> int:
        """Inserts sport into database"""
        if not self.is_already_in_database("Sport", name=name):
            self.database.insert("Sport", ["name"], [f"'{name}'"])
        # returns inserted id
        return self.database.select("Sport", ["id"], f"name = '{name}'")[0][0]

    def insert_modality(self, name: str, sport_id: int) -> int:
        """Inserts modality"""
        if not self.is_already_in_database("Modality", sportID=sport_id, name=name):
            self.database.insert(
                "Modality", ["name", "sportID"], [f"'{name}'", f"'{sport_id}'"]
            )
        # returns inserted id
        return self.database.select(
            "Modality", ["id"], f"name = '{name}' AND sportID = '{sport_id}'"
        )[0][0]

    def insert_event(self, modality_id: int, edition_id: int) -> int:
        """Inserts event into database"""
        if not self.is_already_in_database("Event", modalityID=modality_id, editionId=edition_id):
            self.database.insert(
                "Event",
                ["modalityID", "editionId"],
                [f"'{modality_id}'", f"'{edition_id}'"],
            )
        # returns inserted id
        return self.database.select(
            "Event",
            ["id"],
            f"modalityID = '{modality_id}' AND editionId = '{edition_id}'",
        )[0][0]

    def insert_athlete(self, name: str, athlete_country: str) -> int:
        """Inserts an athlete into the database"""
        # getting the country id
        country_id = self.insert_location(athlete_country)
        # checking if it is not already in database
        if not self.is_already_in_database("Athlete", name=name, countryId=country_id):
            # insert athlete
            self.database.insert(
                "Athlete", ["name", "countryId"], [f"'{name}'", f"{country_id}"]
            )
        # returns the athlete id
        return self.database.select(
            "Athlete",
            ["id"],
            f"name = '{name}' AND countryId = '{country_id}'")[0][0]

    def insert_location(self, name: str) -> int:
        """Inserts a country into the database"""
        if not self.is_already_in_database("Location", name=name):
            self.database.insert("Location", ["name"], [f"'{name}'"])
        # returns the id
        return self.database.select("Location", ["id"], f"name = '{name}'")[0][0]

    def insert_mark(self, value: float, event_id: int, athlete_owner_id: int):
        """Insert mark into the database"""
        if not self.is_already_in_database("Mark", athleteId=athlete_owner_id, eventId=event_id):
            self.database.insert(
                "Mark",
                ["value", "eventId", "athleteId"],
                [f"'{value}'", f"'{event_id}'", f"'{athlete_owner_id}'"],
            )
        # returns inserted id
        return self.database.select(
            "Mark",
            ["id"],
            f"eventId = '{event_id}' AND athleteId = '{athlete_owner_id}'",
        )[0][0]

    def end_connection(self):
        """Ends the connection with the database"""
        self.database.end_connection()
        print("INFO: Connection with the database ended.")
