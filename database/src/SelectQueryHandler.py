from __future__ import annotations
import abc
from typing import List


class Table(abc.ABC):
    """
    An interface to represent the tables in our database.
    Its main responsibility is to construct the select query that can be used to access the data in the database.
    """

    @abc.abstractmethod
    def get_select_query(self) -> str:
        """This method returns the select query to perform the request in the database."""
        pass

    @abc.abstractmethod
    def construct_columns_labels(self) -> list:
        """
        It provides a list of column names selected in the database,
        ordered to match the sequence in which the data is retrieved from the database.

        Example:
             for SELECT name, id from Athlete,
             it will return
             [Athlete.name, Athlete.id]
        """
        pass

    @abc.abstractmethod
    def get_table_alias(self, table_name):
        """
        Returns the alias used for the table in the selected query.

        Example:
            select ath.name, ath.id from Athlete as ath.
            It will return ath.

        This will be used by the WhereClause object.
        """
        pass


class SingleTable(Table):
    """
    Class to represent a single table in the database.
    """

    def __init__(self, table_name: str, *columns, **foreign_keys):
        self.__table_name: str = table_name
        self.__columns: tuple = columns
        # dictionary to stores the foreign_keys.
        # the key must be the name of the foreign key and the value must be the name of the table it is referencing to.
        self.__foreign_keys: dict = foreign_keys

    def __eq__(self, other: SingleTable):
        # checks with the table are the same
        name_check = self.table_name == other.table_name
        columns_check = set(self.columns) == set(other.columns)
        foreign_keys_check = self.foreign_keys == other.foreign_keys
        # they are the same if all features are the same
        return name_check and columns_check and foreign_keys_check

    @property
    def table_name(self):
        return self.__table_name

    @property
    def columns(self):
        return self.__columns

    @property
    def foreign_keys(self):
        return self.__foreign_keys

    def get_select_query(self) -> str:
        return "SELECT " + ", ".join(self.columns) + f" FROM {self.table_name}"

    def construct_columns_labels(self) -> list:
        return [f"{self.table_name}.{column}" for column in self.columns]

    def get_table_alias(self, table_name):
        if table_name == self.table_name:
            return table_name
        else:
            # TODO: in case it's not we need to raise an error
            return None


class JoinTables(Table):
    """
    A class to represent the inner join select command of two of more tables.
    """

    def __init__(self, single_tables: List[SingleTable]):
        # dictionary with the table
        self.__tables = {table.table_name: table for table in single_tables}
        # dictionary to store the table alias
        self.__tables_alias = {table.table_name: None for table in single_tables}

    def get_select_query(self) -> str:
        # select part
        select_part = ", ".join(self.construct_select_part())
        # from part
        from_part = " ".join(self.construct_from_part())
        # return sql query
        return f"SELECT {select_part} {from_part}"

    def find_initial_table(self):
        """
        A method to identify the starting table for constructing the columns to be retrieved from the database.
        The initial table should be the one that has foreign keys but no other table has a reference to it.
        """
        # list of all tables that are refereced
        referenced_tables = sum([list(table.foreign_keys.values()) for _, table in self.__tables.items()], [])

        # if there's only one table, we should return this table
        if len(self.__tables) == 1:
            return self.__tables[list(self.__tables.keys())[0]]

        # searching for the table
        for table_name, table in self.__tables.items():
            if table_name not in referenced_tables and len(table.foreign_keys):
                return table
        # if no table was found, we return none
        return None

    def recursive_constructor(
            self, table: SingleTable, constructor, name_constructor, previous_table: str = "",
            full_path_name: bool = False
    ) -> list:
        """
        Method to construct recursively the select and from part of the query, and also the columns labels.

        :param table: SingleTable object
        :param constructor: a method to construct the queries or a list
        :param name_constructor: a method to construct the name of the table (it will be its alias, for example)
        :param previous_table: name of the previous table
        :param full_path_name: if the full path up to the current table must be taken into account to write the
        table name, or we can just use the name of the previous table.
        """
        # columns of the tables
        table_name = name_constructor(table.table_name, previous_table)
        # constructing the query parts
        query_parts = constructor(table_name, table)
        # loop over the tables that are foreign keys of the current table
        for referenced_table in table.foreign_keys.values():
            # checks if it's one table that we will select data from
            if referenced_table in self.__tables:
                # adding the columns of the foreign key tables
                query_parts += self.recursive_constructor(
                    self.__tables[referenced_table],
                    constructor,
                    name_constructor,
                    (table_name if full_path_name else table.table_name),
                    full_path_name
                )
        return query_parts

    def construct_select_part(self) -> list:
        """Constructs the select part"""
        return self.recursive_constructor(self.find_initial_table(), self.construct_select, self.table_name)

    def construct_from_part(self) -> list:
        """Constructs the 'from' part of the query"""
        initial_table = self.find_initial_table()
        self.__tables_alias[initial_table.table_name] = initial_table.table_name
        return ([f"FROM {initial_table.table_name}"]
                + self.recursive_constructor(initial_table, self.construct_inner_join, self.table_name))

    def construct_columns_labels(self) -> list:
        """Returns the column orders of the select"""
        # we need to find the initial table again
        return self.recursive_constructor(self.find_initial_table(), self.construct_select, self.column_name,
                                          full_path_name=True)

    @staticmethod
    def construct_select(table_name: str, table: SingleTable) -> list:
        """It returns the columns we must select from the database of the table object."""
        return [f"{table_name}.{column}" for column in table.columns]

    def construct_inner_join(self, table_name: str, table: SingleTable) -> list:
        """It constructs the inner join part of the query for table and their foreign_keys."""
        # list to hold the inner join commands
        inner_joins = []
        # loop over the referenced tables
        for forgein_key, referenced_table in table.foreign_keys.items():
            # we only add to the inner join if the referenced table is one of the tables we want to get data from
            if referenced_table in self.__tables:
                # creating an alias for the referenced table
                self.__tables_alias[referenced_table] = f"{table.table_name}_{referenced_table}"
                inner_joins.append(f"INNER JOIN {referenced_table} AS {table.table_name}_{referenced_table} "
                                   f"ON {table_name}.{forgein_key} = {table.table_name}_{referenced_table}.id")
        return inner_joins

    @staticmethod
    def table_name(current_name: str, previous_name: str = ""):
        return previous_name + "_" + current_name if len(previous_name) else current_name

    @staticmethod
    def column_name(current_name: str, previous_name: str = ""):
        return f"{previous_name}.{current_name}" if len(previous_name) else f"{current_name}"

    def get_table_alias(self, table_name):
        if table_name in self.__tables_alias:
            return self.__tables_alias[table_name]


class WhereClause(abc.ABC):
    """
    Abstract class to handle the where clause part of the select query.
    """

    def get_where_clause(self, table: Table) -> str:
        return f"WHERE {self.construct_condition(table)}"

    @abc.abstractmethod
    def construct_condition(self, table: Table) -> str:
        """It must construct the where condition for Table."""
        pass


class SingleWhereCondition(WhereClause):
    """Class to construct a single where clauase with a single condition."""

    def __init__(self, table_name: str, key: str, values: list):
        # name of the table we must use in the where clause
        self.__table_name: str = table_name
        # column key that we will use to construct the condition
        self.__key: str = key
        # values that column is allowed to take
        self.__values: list = values

    def construct_condition(self, table: Table) -> str:
        # find the table alias
        table_alias = table.get_table_alias(self.__table_name)
        # values allowed
        values = ', '.join([f"'{name}'" for name in self.__values])
        # return the contidion
        return f"{table_alias}.{self.__key} IN ({values})"


class ForeignKeyCondition(WhereClause):
    """Condition that selects only records of a table if it's a foreign key of other table."""

    def __init__(self, table_name: str, primary_key: str, reference_table: Table):
        # name of the table
        self.__table_name: str = table_name
        # key that we must check if belongs to a foregin key
        self.__primary_key: str = primary_key
        # table that references the primary_key
        self.__referenced_table: Table = reference_table

    def construct_condition(self, table: Table) -> str:
        # find the table alias
        table_alias = table.get_table_alias(self.__table_name)
        # return the contidion
        return f"{table_alias}.{self.__primary_key} IN ({self.__referenced_table.get_select_query()})"


class ManyWhereCondition(WhereClause):
    """Class to construct a where clause with many conditions."""

    def __init__(self, where_clauses: List[WhereClause]):
        self.__where_clauses: List[WhereClause] = where_clauses

    def construct_condition(self, table: Table) -> str:
        """It returns the conditions"""
        return " AND ".join(where_clause.construct_condition(table) for where_clause in self.__where_clauses)


class SelectQuery:
    """This class constructs the select query by taking into account the Table and WhereClause object."""

    def __init__(self, table: Table, where_clause: WhereClause = None):
        self.__table: Table = table
        self.__where_clause: WhereClause = where_clause

    @property
    def table(self):
        return self.__table

    @property
    def where_clause(self):
        return self.__where_clause

    @table.setter
    def table(self, table: Table):
        self.__table = table

    @where_clause.setter
    def where_clause(self, where: WhereClause):
        self.__where_clause = where

    def get_select_query(self) -> str:
        """It returns the select query"""
        # select part
        select = self.table.get_select_query()
        # where clause if it is needed
        where_clause = self.where_clause.get_where_clause(self.table) if self.where_clause else ""
        # full sql query
        return f"{select} {where_clause}"

    def get_columns_labels(self) -> list:
        return self.table.construct_columns_labels()
