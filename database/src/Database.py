import abc


class Database(abc.ABC):
    """
    Interface for the classes that interact with a database.
    """

    @abc.abstractmethod
    def __init__(self, usr: str, pwd: str, host: str, port: int, db: str):
        """Saves connection parameters"""
        pass

    @abc.abstractmethod
    def connect(self) -> bool:
        """Returns True if the connection is successful"""
        pass

    @abc.abstractmethod
    def end_connection(self) -> bool:
        """Returns True if the connection is closed successfully"""
        pass

    @abc.abstractmethod
    def select(self, table: str, columns: list, where_clause: str) -> list:
        """Returns a list of the selected elements"""
        pass

    @abc.abstractmethod
    def insert(self, table: str, columns: list, values: list) -> None:
        """Inserts data in a table"""
        pass

    @abc.abstractmethod
    def update(self, table: str, columns: list, values: list, where_clause: str) -> None:
        """Updates data in a table"""
        pass

    @abc.abstractmethod
    def delete(self, table: str, where_clause: str) -> None:
        """Deletes data from a table"""
        pass

    @abc.abstractmethod
    def execute_query(self, query: str, insertion_query: bool = False) -> list:
        """Executes a specific query"""
        pass
