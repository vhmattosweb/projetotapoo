import pytest
from database.src.OlympicDataRequest import OlympicDataRequest


class TestOlympicDataRequest:
    """Tests the OlympicDataRequest class"""

    def test_integration(self, mocker):
        """
        Tests if the OlympicDataRequest is properly calling the SelectQuery and Database classes.
        """
        # creating the mocks for the objects we need to replace
        db_mocker = mocker.MagicMock()
        select_query_mocker = mocker.MagicMock()

        # return values of each mocked class
        select_query_output = "SELECT name, id FROM Athlete"
        select_query_columns = ["Athlete.name", "Athlete.id"]
        db_output = [("Matheus", 1), ("Marilia", 2)]
        mocker.patch.object(select_query_mocker, "get_select_query", return_value=select_query_output)
        mocker.patch.object(select_query_mocker, "get_columns_labels", return_value=select_query_columns)
        mocker.patch.object(db_mocker, "execute_query", return_value=db_output)

        # creating the DataRequest object and calling the request_data method
        olymp_data_request = OlympicDataRequest(select_query_mocker, db_mocker)
        olymp_data_request.request_data()

        # checking if the methods were only called once
        select_query_mocker.get_select_query.assert_called_once()
        select_query_mocker.get_columns_labels.assert_called_once()
        db_mocker.execute_query.assert_called_once()

        # records and expected result
        records = olymp_data_request.get_records()

        # comparing the results
        first_record = records[0].athlete.name == "Matheus" and records[0].athlete.id == 1
        second_record = records[1].athlete.name == "Marilia" and records[1].athlete.id == 2

        assert first_record and second_record
