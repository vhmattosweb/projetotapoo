import pytest


class TestSingleWhereClause:

    def test_athlete_table(self, athlete_table, athlete_condition):
        assert athlete_condition.construct_condition(athlete_table) == "Athlete.id IN ('1', '2', '3')"

    def test_join_table(self, join_table, location_condition):
        assert location_condition.construct_condition(join_table) == "Athlete_Location.id IN ('1', '2', '3')"

    def test_athlete_name(self, athlete_table, athlete_condition_name):
        assert athlete_condition_name.construct_condition(athlete_table) == ("Athlete.name IN ('Cesar Cielo', "
                                                                             "'Bruno Fratus')")

    def test_join_table_name(self, join_table, location_condition_name):
        assert location_condition_name.construct_condition(join_table) == "Athlete_Location.name IN ('BRA')"

    def test_foreign_key_condition(self, location_table, foreign_key_condition):
        assert foreign_key_condition.construct_condition(location_table) == ("Location.id IN "
                                                                             "(SELECT countryId FROM Athlete)")


class TestManyWhereCondition:

    def test_many_condition(self, join_table, many_conditions):
        assert many_conditions.construct_condition(join_table) == ("Athlete_Location.id IN ('1', '2', '3') "
                                                                   "AND Athlete.id IN ('1', '2', '3')")

    def test_many_condition_with_names(self, join_table, many_conditions_names):
        assert many_conditions_names.construct_condition(join_table) == ("Athlete_Location.name IN ('BRA') AND "
                                                                         "Athlete.name IN ('Cesar Cielo', "
                                                                         "'Bruno Fratus')")

    def test_many_mixed_conditions(self, join_table, mixed_conditions):
        assert mixed_conditions.construct_condition(join_table) == ("Athlete_Location.name IN ('BRA') AND Athlete.id "
                                                                    "IN ('1', '2', '3')")
