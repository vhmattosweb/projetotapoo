import pytest
import database.src.SelectQueryHandler as query_handler


class TestSingleTable:

    @pytest.fixture
    def single_query(self):
        return query_handler.SingleTable("Athlete", "id", "name", countryId="Location")

    def test_table_name(self, single_query):
        assert single_query.table_name == "Athlete"

    def test_columns(self, single_query):
        assert single_query.columns == ("id", "name")

    def test_foreign_keys(self, single_query):
        assert single_query.foreign_keys == {"countryId": "Location"}

    def test_get_select_query(self, single_query):
        assert single_query.get_select_query() == "SELECT id, name FROM Athlete"

    def test_table_alias(self, single_query):
        assert single_query.get_table_alias("Athlete") == "Athlete"
