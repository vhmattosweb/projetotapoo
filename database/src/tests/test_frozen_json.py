"""Tests the FrozenJSON class"""

import pytest
from database.src.FrozenJSON import FrozenJSON


class TestFrozenJson:

    def setup_method(self):
        self.simple_dict = {
            "name": "Matheus",
            "NUSP": 9797145,
            "program": "PhD",
            "areas": ["particle physics", "higgs physics"]
        }
        self.frozen_json = FrozenJSON(self.simple_dict)

    def test_name(self):
        assert self.frozen_json.name == self.simple_dict["name"]

    def test_nusp(self):
        assert self.frozen_json.nusp == self.simple_dict["NUSP"]

    def test_program(self):
        assert self.frozen_json.program == self.simple_dict["program"]

    def test_areas(self):
        assert self.frozen_json.areas == self.simple_dict["areas"]


class TestNestedFrozenJson:

    def setup_method(self):

        self.nested_dict = {
            "name": "Matheus",
            "classes": {"Physics": "QFT 1", "Mathematics": "Calculus", "Computation": "TAPOO"},
            "classmates": {
                "Marilia": "Masters", "Victor": "Masters"
            }
        }

        self.frozen_json = FrozenJSON(self.nested_dict)

    def test_classes_physics(self):
        assert self.frozen_json.classes.physics == self.nested_dict["classes"]["Physics"]

    def test_classes_mathematics(self):
        assert self.frozen_json.classes.mathematics == self.nested_dict["classes"]["Mathematics"]

    def test_classes_computation(self):
        assert self.frozen_json.classes.computation == self.nested_dict["classes"]["Computation"]

    def test_classmates_marilia(self):
        assert self.frozen_json.classmates.marilia == self.nested_dict["classmates"]["Marilia"]

    def test_classmates_victor(self):
        assert self.frozen_json.classmates.victor == self.nested_dict["classmates"]["Victor"]

