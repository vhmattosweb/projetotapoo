import pytest
import database.src.SelectQueryHandler as query_handler


class TestSelectQuery:
    """
    Testing the SelectQuery class
    """

    def test_calls_to_depended_classes(self, mocker):
        """
        Checks wether the calls to the Table and WhereClause have been made.
        """
        # creating the mocks for the Table and WhereClause class
        table_mocker = mocker.MagicMock()
        where_clause_mocker = mocker.MagicMock()

        # setting up the return values for the methods that the SelectQuery will call
        mocker.patch.object(table_mocker, "get_select_query", return_value="SELECT id FROM Test")
        mocker.patch.object(where_clause_mocker, "get_where_clause", return_value="WHERE Test.id IN ('1')")

        # creating the SelectQuery object with the Mocks
        select_query = query_handler.SelectQuery(table_mocker, where_clause_mocker)

        # calling the method that will call mocked methods
        generated_query = select_query.get_select_query()

        # checking if the methods were called only one time
        table_mocker.get_select_query.assert_called_once()
        where_clause_mocker.get_where_clause.assert_called_once()

        assert generated_query == "SELECT id FROM Test WHERE Test.id IN ('1')"

    @staticmethod
    def select_query_test_method(table, where_condition):
        """
        Tests if the resulted SQL query is correct for generic a Table and WhereClause.
        """
        select_query = query_handler.SelectQuery(table, where_condition)

        # expected result that the query should have
        expected_select_query = f"{table.get_select_query()} {where_condition.get_where_clause(table)}"

        assert select_query.get_select_query() == expected_select_query

    def test_select_query_athlete(self, athlete_table, athlete_condition):
        self.select_query_test_method(athlete_table, athlete_condition)

    def test_select_query_location(self, location_table, location_condition):
        self.select_query_test_method(location_table, location_condition)

    def test_select_query_inner_join(self, join_table, location_condition):
        self.select_query_test_method(join_table, location_condition)

    def test_select_query_many_conditions(self, join_table, many_conditions):
        self.select_query_test_method(join_table, many_conditions)

    def test_select_query_many_conditions_names(self, join_table, many_conditions_names):
        self.select_query_test_method(join_table, many_conditions_names)

    def test_select_query_mixed_conditions(self, join_table, mixed_conditions):
        self.select_query_test_method(join_table, mixed_conditions)

    def test_select_query_foreign_key(self, location_table, foreign_key_condition):
        self.select_query_test_method(location_table, foreign_key_condition)
