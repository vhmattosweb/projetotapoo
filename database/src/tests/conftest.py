import pytest
import database.src.SelectQueryHandler as query_handler


@pytest.fixture
def athlete_table():
    return query_handler.SingleTable("Athlete", "name", "id", countryId="Location")


@pytest.fixture
def athlete_condition():
    return query_handler.SingleWhereCondition("Athlete", "id", [1, 2, 3])


@pytest.fixture
def athlete_condition_name():
    return query_handler.SingleWhereCondition("Athlete", "name", ["Cesar Cielo", "Bruno Fratus"])


@pytest.fixture
def location_table():
    return query_handler.SingleTable("Location", "name", 'id')


@pytest.fixture
def location_condition():
    return query_handler.SingleWhereCondition("Location", "id", [1, 2, 3])


@pytest.fixture
def location_condition_name():
    return query_handler.SingleWhereCondition("Location", "name", ["BRA"])


@pytest.fixture
def many_conditions(location_condition, athlete_condition):
    return query_handler.ManyWhereCondition([location_condition, athlete_condition])


@pytest.fixture
def many_conditions_names(location_condition_name, athlete_condition_name):
    return query_handler.ManyWhereCondition([location_condition_name, athlete_condition_name])


@pytest.fixture
def mixed_conditions(location_condition_name, athlete_condition):
    return query_handler.ManyWhereCondition([location_condition_name, athlete_condition])


@pytest.fixture
def join_table(athlete_table, location_table):
    join_table = query_handler.JoinTables([athlete_table, location_table])
    _ = join_table.get_select_query()
    return join_table


@pytest.fixture
def athlete_location_foreign_key():
    return query_handler.SingleTable("Athlete", "countryId")


@pytest.fixture
def foreign_key_condition(athlete_location_foreign_key):
    return query_handler.ForeignKeyCondition("Location", "id", athlete_location_foreign_key)
