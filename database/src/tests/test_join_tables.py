import pytest
import database.src.SelectQueryHandler as query_handler
from database.src.TableFactory import SingleTableFactory

# dictionary of all table in our database
tables_dictionary = {
    "Athlete": {"columns": ["id", "name"], "foreign_keys": {"countryId": "Location"}},
    "Location": {"columns": ["id", "name"], "foreign_keys": {}},
    "Mark": {
        "columns": ["value"],
        "foreign_keys": {"eventId": "Event", "athleteId": "Athlete"},
    },
    "Event": {
        "columns": [],
        "foreign_keys": {"editionId": "EditionData", "modalityID": "Modality"},
    },
    "EditionData": {
        "columns": ["id", "name", "year"],
        "foreign_keys": {"cityId": "Location"},
    },
    "Modality": {"columns": ["id", "name"], "foreign_keys": {"sportID": "Sport"}},
    "Sport": {"columns": ["id", "name"], "foreign_keys": {}},
}


class TestJoinTables:
    """Simple tests on the JoinTables class."""

    def setup_method(self):
        # single table factory object
        self.single_table_factory = SingleTableFactory()
        # list of single tables
        self.single_tables_dict = {
            table_name: self.single_table_factory.create_table(
                table_name, *features["columns"], **features["foreign_keys"]
            )
            for table_name, features in tables_dictionary.items()
        }
        # creating the join table
        self.join_table = query_handler.JoinTables(list(self.single_tables_dict.values()))
        # creating the select query
        _ = self.join_table.get_select_query()

    def test_initial_table(self):
        # initial table must be the Mark table
        assert self.join_table.find_initial_table() == self.single_tables_dict["Mark"]

    def test_table_name(self):
        assert self.join_table.table_name("Location", "Athlete") == "Athlete_Location"

    def test_construct_select(self):
        select_list = ["Athlete.id", "Athlete.name"]
        assert self.join_table.construct_select("Athlete", self.single_tables_dict["Athlete"]) == select_list

    def test_recursive_constructor_select(self):
        columns = self.join_table.recursive_constructor(
            self.single_tables_dict["Modality"], self.join_table.construct_select, self.join_table.table_name
        )
        expected_columns = ["Modality.id", "Modality.name", "Modality_Sport.id", "Modality_Sport.name"]
        assert columns == expected_columns

    def test_select_part(self):
        select_columns = [
            'Mark.value',
            'Event_EditionData.id',
            'Event_EditionData.name',
            'Event_EditionData.year',
            'EditionData_Location.id',
            'EditionData_Location.name',
            'Event_Modality.id',
            'Event_Modality.name',
            'Modality_Sport.id',
            'Modality_Sport.name',
            'Mark_Athlete.id',
            'Mark_Athlete.name',
            'Athlete_Location.id',
            'Athlete_Location.name'
        ]
        assert self.join_table.construct_select_part() == select_columns

    def test_construct_inner_join(self):
        expected_result = [
            'INNER JOIN Event AS Mark_Event ON Mark.eventId = Mark_Event.id',
            'INNER JOIN Athlete AS Mark_Athlete ON Mark.athleteId = Mark_Athlete.id'
        ]
        result_got = self.join_table.construct_inner_join("Mark", self.single_tables_dict["Mark"])
        assert result_got == expected_result

    def test_columns_labels(self):
        expected_result = [
            'Mark.value', 'Mark.Event.EditionData.id', 'Mark.Event.EditionData.name', 'Mark.Event.EditionData.year',
            'Mark.Event.EditionData.Location.id', 'Mark.Event.EditionData.Location.name', 'Mark.Event.Modality.id',
            'Mark.Event.Modality.name', 'Mark.Event.Modality.Sport.id', 'Mark.Event.Modality.Sport.name',
            'Mark.Athlete.id', 'Mark.Athlete.name', 'Mark.Athlete.Location.id', 'Mark.Athlete.Location.name'
        ]
        assert self.join_table.construct_columns_labels() == expected_result


class ColumnsGenerator:
    """Class to generate the expected behavior of construct_select_part method from JoinTables class."""

    def __init__(self):
        # dictionary with all tables
        self.tables_dictionary = dict()
        # list to store the columns that the select must generate
        self.list_columns = []
        # list to store the FROM part that the select must generate
        self.list_inner_join = []

    def set_tables_dictionary(self, tables: dict):
        self.tables_dictionary = tables

    def generate_list_columns(self, table_name: str, previous_table: str = ""):
        # generating table alias
        table_alias = f"{previous_table}_{table_name}" if len(previous_table) else table_name
        # updating list
        self.update_list_columns(table_name, table_alias)
        # constructing the other columns of the foreing keys
        for foreign_table in self.tables_dictionary[table_name]["foreign_keys"].values():
            if foreign_table in self.tables_dictionary:
                self.generate_list_columns(foreign_table, table_name)

    def update_list_columns(self, table_name: str, table_alias: str):
        for column in self.tables_dictionary[table_name]["columns"]:
            self.list_columns.append(f"{table_alias}.{column}")

    def generate_inner_join(self, table_name: str, previous_table: str = ""):
        # generating table alias
        table_alias = f"{previous_table}_{table_name}" if len(previous_table) else table_name
        # updating the list
        self.update_inner_join_list(table_name, table_alias)
        # constructing the other columns of the foreing keys
        for foreign_table in self.tables_dictionary[table_name]["foreign_keys"].values():
            if foreign_table in self.tables_dictionary:
                self.generate_inner_join(foreign_table, table_name)

    def update_inner_join_list(self, table_name: str, table_alias: str):
        # loop over the foreign keys
        for foreign_key, table in self.tables_dictionary[table_name]["foreign_keys"].items():
            if table in self.tables_dictionary:
                self.list_inner_join.append(
                    f"INNER JOIN {table} AS {table_name}_{table} ON "
                    f"{table_alias}.{foreign_key} = {table_name}_{table}.id"
                )

    def get_columns(self):
        return self.list_columns

    def get_inner_join_list(self):
        return self.list_inner_join


class TestColumnsGeneration:
    """Tests for the column generation."""

    # combination we wish to cross-check
    combinations_to_check = [
        ("Athlete", "Location"),
        ("EditionData", "Location"),
        ("Modality", "Sport"),
        ("Event", "EditionData", "Modality", "Sport"),
        ("Athlete",),
        ("Location",),
        ("Mark", "Event", "Athlete", "EditionData"),
        ("Modality",),
        ("EditionData",),
        ("Mark", "Athlete", "Location", "EditionData", "Event", "Modality", "Sport")
    ]
    initial_tables = [
        "Athlete", "EditionData", "Modality", "Event", "Athlete", "Location", "Mark", "Modality", "EditionData",
        "Mark"
    ]

    # creating the list with dictionaries
    dictionary_list = [
        {table: tables_dictionary[table] for table in list_tables} for list_tables in combinations_to_check
    ]

    # creating the list of tuples with the dictionaries and the initial tables names
    tables_tuple = [
        (dict_table, table_name) for dict_table, table_name in zip(dictionary_list, initial_tables)
    ]

    def setup_method(self):
        self.columns_generator = ColumnsGenerator()

    @staticmethod
    def create_join_table(tables_dict):
        # single table factory
        table_factory = SingleTableFactory()
        # creating the tables
        tables_list = [table for table in table_factory.create_tables_from_dict(tables_dict)]
        # creating the join tables
        join_tables = query_handler.JoinTables(tables_list)
        return join_tables

    @pytest.mark.parametrize("tables_dict,table_name", tables_tuple)
    def test_find_initial_table(self, tables_dict, table_name):
        # creating the join table
        join_table = self.create_join_table(tables_dict)
        # checking the initial table found
        assert join_table.find_initial_table().table_name == table_name

    @pytest.mark.parametrize("tables_dict", dictionary_list)
    def test_columns_generation(self, tables_dict):
        # creating the join tables
        join_tables = self.create_join_table(tables_dict)
        initial_table = join_tables.find_initial_table()
        # generating the expected behavior
        self.columns_generator.set_tables_dictionary(tables_dict)
        self.columns_generator.generate_list_columns(initial_table.table_name)
        # checking the select part
        assert join_tables.construct_select_part() == self.columns_generator.get_columns()

    @pytest.mark.parametrize("tables_dict", dictionary_list)
    def test_inner_join_constructor(self, tables_dict):
        # creating the join tables
        join_tables = self.create_join_table(tables_dict)
        initial_table = join_tables.find_initial_table()
        # generating the expected behavior
        self.columns_generator.set_tables_dictionary(tables_dict)
        self.columns_generator.generate_inner_join(initial_table.table_name)
        # expected result
        expected = [f"FROM {initial_table.table_name}"] + self.columns_generator.get_inner_join_list()
        # checking the select part
        assert join_tables.construct_from_part() == expected

    @pytest.mark.parametrize("tables_dict", dictionary_list)
    def test_select_query(self, tables_dict):
        # creating the join tables
        join_tables = self.create_join_table(tables_dict)
        initial_table = join_tables.find_initial_table()
        # generating the expected behavior
        self.columns_generator.set_tables_dictionary(tables_dict)
        self.columns_generator.generate_list_columns(initial_table.table_name)
        self.columns_generator.generate_inner_join(initial_table.table_name)
        # expected select part
        expected_select_part = ", ".join(self.columns_generator.get_columns())
        # expected from part result
        expected_from_part = [f"FROM {initial_table.table_name}"] + self.columns_generator.get_inner_join_list()
        expected_from_part = " ".join(expected_from_part)
        # cheking the results
        assert join_tables.get_select_query() == f"SELECT {expected_select_part} {expected_from_part}"
