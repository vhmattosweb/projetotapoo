import abc


class OlympicDataInserter(abc.ABC):
    """Interface to insert the scraped data into the dabase"""

    """Create the insert methods for Event, Modality and Sport"""

    @abc.abstractmethod
    def is_already_in_database(self, table: str, value: str) -> bool:
        """Checks wheter value is already in table"""
        pass

    @abc.abstractmethod
    def insert_edition(self, name: str, year: str, city_name: str) -> int:
        """Inserts new edition"""
        pass

    @abc.abstractmethod
    def insert_sport(self, name: str) -> int:
        """Inserts new sport"""
        pass

    @abc.abstractmethod
    def insert_modality(self, name: str, sport_id: int) -> int:
        """Inserts new modality"""
        pass

    @abc.abstractmethod
    def insert_event(self, modality_id: int, edition_id: int) -> int:
        """Inserts a new event"""
        pass

    @abc.abstractmethod
    def insert_athlete(self, name: str, athlete_country: str) -> int:
        """Inserts an athlete into the database"""
        pass

    @abc.abstractmethod
    def insert_location(self, name: str) -> int:
        """Inserts a country into the database"""
        pass

    @abc.abstractmethod
    def insert_mark(self, value: float, event_id: int, athlete_owner_id: int) -> int:
        """Insert mark into the database"""
        pass

    @abc.abstractmethod
    def end_connection(self):
        """Ends the connection with the database"""
        pass
