import database.src.SelectQueryHandler as query_handler


class SingleTableFactory:
    """
    Class responsible for creating SingleTable objects.
    It can be a singleton since one instance can create any type of SingleTable object.
    """

    # variable to store the instance
    instance = None

    def __new__(cls):
        """This is required to ensure we are implementing a Singleton"""
        if cls.instance is None:
            cls.instance = super(SingleTableFactory, cls).__new__(cls)
        return cls.instance

    def create_tables_from_dict(self, tables_dict: dict):
        """
        Create SingleTable objects from a dictionary and return a generator over them.
        The dictionary must follow the following structure:

        tables_dict = {
            'name_of_table': {
                'columns': ['column1', 'column2', ...],
                'foreign_keys': {
                    'foreign_key1_name': 'referenced_table',
                    ...
                }
            },
            ...
        }
        """
        # loop over the tables
        for table_name, columns in tables_dict.items():
            yield self.create_table(
                table_name, *columns["columns"], **columns["foreign_keys"]
            )

    @staticmethod
    def create_table(
        table_name: str, *columns, **foreign_keys
    ) -> query_handler.SingleTable:
        """Creates a SingleTable object."""
        return query_handler.SingleTable(table_name, *columns, **foreign_keys)
