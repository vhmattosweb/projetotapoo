"""
Implementation of FrozenJSON class with minor modifications.
This class was taken from the book Fluent Python by Luciano Ramalho, Chapter 22.
"""
from collections import abc


class FrozenJSON:
    """Class to read-only JSON objects or any kind of mapping such as a python dictionary"""

    def __new__(cls, arg):
        # if the argument is a dictionary (or any type of Mapping), we create an instance
        if isinstance(arg, abc.Mapping):
            return super().__new__(cls)
        # if it's a list we return a list of FrozenJSONs (this part is done using recursion)
        elif isinstance(arg, abc.MutableSequence):
            return [cls(element) for element in arg]
        else:
            return arg

    def __init__(self, mapping):
        self.__data = {}
        for key, value in mapping.items():
            self.__data[self.fix_key(key)] = value

    @classmethod
    def fix_key(cls, key: str) -> str:
        """
        Fix the key.
        Proper order: (men or women)_discipline_distance
        """
        # creating a list with all words in the key
        split_tag = "-" if "-" in key else " "
        all_words = [word.lower() for word in key.split(split_tag) if len(word) > 1]
        # checking if we have gender specification
        gender = [word == "men" or word == "women" for word in all_words]
        try:
            right_order = [all_words[gender.index(True)]]
            all_words.remove(all_words[gender.index(True)])
        except ValueError:
            right_order = []
        # moving the string with digits to the end
        for word in all_words[:-1]:
            if word[0].isdigit():
                all_words.append(all_words.pop(all_words.index(word)))
        # returns the key
        return "_".join(right_order + all_words)

    def __getattr__(self, name):
        try:
            # checks if the attribute is one of the dict attrs (Ex: keys())
            return getattr(self.__data, name)
        except AttributeError:
            # if it's not we return a FrozenJSON with the content inside data[name]
            return FrozenJSON(self.__data[name])

    def __dir__(self):
        return self.__data.keys()

    def __repr__(self):
        return self.__data.__repr__()

    def __iter__(self):
        for item in self.__data:
            yield item, FrozenJSON(self.__data[item])

    def __getitem__(self, item):
        return FrozenJSON(self.__data[item])

    def __dict__(self):
        return self.__data
