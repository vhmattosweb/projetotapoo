from database.src.FrozenJSON import FrozenJSON
import json

if __name__ == "__main__":
    file_ = json.load(open("/home/matheus/ProjetoTAPOO/database/data/ALL_Olympic_Games_Results.json"))
    data = FrozenJSON(file_)

    # displaying all editions
    for edition, _ in data:
        print(edition)
    print("------------------------------------------------------------------")
    # getting sports
    for sport, _ in data.tokyo_2020:
        print(sport)
    print("------------------------------------------------------------------")
    # getting all modalities
    for modality, _ in data.beijing_2008.athletics:
        print(modality)
    print("------------------------------------------------------------------")
    # getting results from a specific modality
    for mark in data.beijing_2008.swimming.men_freestyle_50m:
        print(mark.athlete, mark.country, mark.result)
