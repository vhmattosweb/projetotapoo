import json
from database.src.FrozenJSON import FrozenJSON
from database.src.Insertion import Insertion
from database.src.MySQL import MySQL
from database.src.OlympicDataInserterAdapter import OlympicDataInserterAdapter as oda


def main():
    # reading the json file
    json_file = json.load(open("/home/matheus/ProjetoTAPOO/database/data/ALL_Olympic_Games_Results.json"))
    # initializing the FrozenJson object
    olympic_data = FrozenJSON(json_file)
    # initializing the database
    olympic_database = MySQL("olympicuser", "tap00lympics", "132.226.240.196", 3306, "tapoo")
    # creating the adapter object that we can insert all data into the database
    olypic_data_insertor_adapter = oda(olympic_database)
    # initializing the insertion object
    insertion = Insertion(olympic_data, olypic_data_insertor_adapter)
    # run insertion
    insertion.run_insertion()
    # close connection
    olypic_data_insertor_adapter.end_connection()


if __name__ == "__main__":
    main()
