"""
Example: select the athlete information using the OlympicDataRequest class.
"""

from database.src.MySQL import MySQL
import database.src.SelectQueryHandler as query_handler
from database.src.OlympicDataRequest import OlympicDataRequest


def main():
    # connection with database
    olympic_database = MySQL("olympicuser", "tap00lympics", "132.226.240.196", 3306, "tapoo")

    # creating the tables
    edition = query_handler.SingleTable("EditionData", "id", "name", cityId="Location")
    location = query_handler.SingleTable("Location", "name")
    joined_tables = query_handler.JoinTables([edition, location])

    # select object
    select = query_handler.SelectQuery(joined_tables)

    print(select.get_select_query())

    # making the request
    request_data = OlympicDataRequest(select, olympic_database)
    request_data.request_data()

    # getting the records
    countries_records = request_data.get_records()

    # show results
    for record in countries_records:
        print(f"{record.editiondata.name}, {record.editiondata.id}")


if __name__ == "__main__":
    main()
