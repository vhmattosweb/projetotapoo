"""
Example: select the athlete information using the OlympicDataRequest class.
"""

from database.src.MySQL import MySQL
import database.src.SelectQueryHandler as query_handler
from database.src.OlympicDataRequest import OlympicDataRequest


def main():
    # connection with database
    olympic_database = MySQL("olympicuser", "tap00lympics", "132.226.240.196", 3306, "tapoo")

    # creating the tables
    athlete = query_handler.SingleTable("Athlete", "name", countryId="Location")
    location = query_handler.SingleTable("Location", "name")
    # creating the inner join table
    join_table = query_handler.JoinTables([athlete, location])

    # creating where clause to choose only athletes from the USA and BRA
    where_clause = query_handler.SingleWhereCondition("Location", "name", ["BRA", "USA"])
    # select object
    select = query_handler.SelectQuery(join_table, where_clause)

    # making the request
    request_data = OlympicDataRequest(select, olympic_database)
    request_data.request_data()

    # getting the records
    athlete_records = request_data.get_records()

    # show results
    for record in athlete_records:
        print(f"{record.athlete.name} ({record.athlete.location.name})")


if __name__ == "__main__":
    main()
