"""
Example: Selects the marks from a modality using the OlympicDataRequest class.
"""

from database.src.MySQL import MySQL
import database.src.SelectQueryHandler as query_handler
from database.src.OlympicDataRequest import OlympicDataRequest


if __name__ == "__main__":    # creating tables
    # athlete = query_handler.SingleTable("Athlete", "name", countryId="Location")
    # location = query_handler.SingleTable("Location", "name")
    # mark = query_handler.SingleTable("Mark", "value", eventId="Event", athleteId="Athlete")
    # event = query_handler.SingleTable("Event", editionId="EditionData", modalityID="Modality")
    # editionData = query_handler.SingleTable("EditionData", "name", "year", cityId="Location")
    modality = query_handler.SingleTable("Modality", "name", "id",  sportID="Sport")
    sport = query_handler.SingleTable("Sport", "name", "id")
    # creatin the join table object
    join_table = query_handler.JoinTables(
        # [athlete, location, mark, event, editionData, modality, sport],
        [modality, sport]
    )

    # creating the where clauses
    # choosing to show th 50m freestyle
    # choosing_modality = query_handler.SingleWhereCondition("Modality", "name", ["men_freestyle_50m"])
    choosing_sport = query_handler.SingleWhereCondition("Sport", "name", ["Swimming"])
    # where_clause = query_handler.ManyWhereCondition([choosing_sport, choosing_modality])

    # creating the select object
    # select = query_handler.SelectQuery(join_table, where_clause)
    select = query_handler.SelectQuery(join_table, choosing_sport)

    # connection with database
    olympic_database = MySQL("olympicuser", "tap00lympics", "132.226.240.196", 3306, "tapoo")

    # making the request
    request_data = OlympicDataRequest(select, olympic_database)
    request_data.request_data()

    # getting the records
    marks_records = request_data.get_records()

    # show results
    for record in marks_records:
        print(record)
    #     print(record)
    #     # print(
        #     f"{record.mark.athlete.name} ({record.mark.athlete.location.name}) - {record.mark.event.editiondata.name} "
        #     f"- {record.mark.value}"
        # )
