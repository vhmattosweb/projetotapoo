import database.src.SelectQueryHandler as sqh
from database.src.MySQL import MySQL

if __name__ == "__main__":
    # creating tables
    athlete = sqh.SingleTable("Athlete", "name", countryId="Location")
    location = sqh.SingleTable("Location", "name")
    mark = sqh.SingleTable("Mark", "value", eventId="Event", athleteId="Athlete")
    event = sqh.SingleTable("Event", editionId="EditionData", modalityID="Modality")
    editionData = sqh.SingleTable("EditionData", "name", "year", cityId="Location")
    modality = sqh.SingleTable("Modality", "name", sportID="Sport")
    sport = sqh.SingleTable("Sport", "name")

    # creating the join table
    join_table = sqh.JoinTables(
        [athlete, location, mark, event, editionData, modality, sport]
    )

    # adding where clause
    choosing_modality = sqh.SingleWhereCondition("Modality", "name", ["men_freestyle_50m"])
    choosing_edition = sqh.SingleWhereCondition("EditionData", "name", ["Tokyo 2020"])
    # creating full where condition
    where_clause = sqh.ManyWhereCondition([choosing_modality, choosing_edition])

    # creating the select
    select = sqh.SelectQuery(join_table, where_clause)

    print("SQL query generated:")
    print(select.get_select_query())

    # testing query
    olympic_database = MySQL("olympicuser", "tap00lympics", "132.226.240.196", 3306, "tapoo")
    olympic_database.connect()
    print("records in database:")
    for record in olympic_database.execute_query(select.get_select_query()):
        print(record)
    olympic_database.end_connection()

    print("Columns orders is given by the following order:")
    print(select.get_columns_labels())
