import database.src.SelectQueryHandler as sqh
from database.src.MySQL import MySQL

if __name__ == "__main__":
    # creating tables
    athlete = sqh.SingleTable("Athlete", "name", countryId="Location")
    location = sqh.SingleTable("Location", "name")
    # creating the join table
    join_table = sqh.JoinTables([athlete, location])
    # creating the select query
    select = sqh.SelectQuery(join_table)
    # adding where clause
    where_clause = sqh.SingleWhereCondition("Location", "name",
                                            ["BRA", "USA", "ITA", "JAM"])
    # adding the where clause
    select.where_clause = where_clause

    print("SQL query generated:")
    print(select.get_select_query())

    # testing query
    olympic_database = MySQL("olympicuser", "tap00lympics", "132.226.240.196", 3306, "tapoo")
    olympic_database.connect()
    print("records in database:")
    for record in olympic_database.execute_query(select.get_select_query()):
        print(record)
    olympic_database.end_connection()
