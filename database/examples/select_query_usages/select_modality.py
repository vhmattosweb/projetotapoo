import database.src.SelectQueryHandler as sqh
from database.src.MySQL import MySQL

if __name__ == "__main__":
    # creating the athlete table
    modality = sqh.SingleTable("Modality", "id", "name", sportID="Sport")
    sport = sqh.SingleTable("Sport", "name", "id")
    inner_join = sqh.JoinTables([modality, sport])
    choosing_sport = sqh.SingleWhereCondition("Sport", "name", ["Swimming", "Athletics"])
    # creating the select query
    select = sqh.SelectQuery(inner_join, choosing_sport)

    print("SQL query generated:")
    print(select.get_select_query())

    # testing query
    olympic_database = MySQL("olympicuser", "tap00lympics", "132.226.240.196", 3306, "tapoo")
    olympic_database.connect()
    print("records in database:")
    for record in olympic_database.execute_query(select.get_select_query()):
        print(record)
    olympic_database.end_connection()
