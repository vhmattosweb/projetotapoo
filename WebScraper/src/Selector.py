import abc
from WebScraper.src.HTML_Loader import HTML_Loader
from WebScraper.src import Exceptions
from WebScraper.src.Scraper import Scraper
from bs4 import BeautifulSoup



class Selector(abc.ABC):
    """Abstract class to select the data from a given web page"""

    def __init__(self):
        # variable to store the HTML_Loader object
        self.__html = None

    @property
    def html(self):
        return self.__html

    @html.setter
    def html(self, html: HTML_Loader):
        self.__html = html

    @abc.abstractmethod
    def select_content(self, identifier, **kwargs):
        """
        It must return a generator over all the selected content found.

        :param identifier: must be something that we will use to identify the data we need to scrape (html tag for
        example)
        :param kwargs: all other named attributes the tag must have for example.
        """
        pass


class JsonSelector(Selector):
    """Implements the selector to read .json files"""

    def select_content(self, identifier, **kwargs):
        """
        Reads the all editions from the json file.
        Unfortunaley, we can't do much here, beasides choosing the season.
        """
        # reading the json file
        json_file = self.html.get_request_result().json()
        # the season
        season = identifier
        # getting the json
        content = json_file["modules"][-1]["content"]
        # getting all summer editions
        return (c["slug"] for c in content if c["season"].lower() == season)


class TagSelector(Selector):
    """Implementation of the Selector class for the cases when we need to scrape data from the HTML content"""

    def __init__(self):
        super().__init__()
        # variable to store the scraper
        self.__scraper = None
        # to store the BeatifulSoup obj
        self.__soup = None
        # variable to store the tags we scraped from the html page
        self.__tags = list()

    @property
    def scraper(self):
        return self.__scraper

    @scraper.setter
    def scraper(self, scraper: Scraper):
        self.__scraper = scraper

    def search_tag(self, tag: str, attrs: dict):
        """
        Search for tag in the HTML file and stores the results in the tags attr.

        :param tag: name of the tag we want to search
        :param attrs: possible features the tag must satisfy.
            Example: {'class': ['styles__CountryName-sc-1r5phm6-1', 'eQULfE'] }
        """
        # generating the BeatifulSoup object
        self.__soup = BeautifulSoup(self.html.get_request_result().text, "html.parser")
        # dictionary if the attrs
        try:
            # loop over all the tags
            self.__tags = self.__soup.find_all(tag, attrs=attrs)
            # checking if the tag was found
            if len(self.__tags) == 0:
                raise Exceptions.TagNotFound
            if len(self.__tags) > 1:
                raise Exceptions.MultipleTagsFound
        except Exceptions.TagNotFound:
            print("Warning: Tag '%s' not found" % tag)
        except Exceptions.MultipleTagsFound:
            print("Warning: Tag '%s' found more than one time" % tag)

    def select_content(self, tag, **kwargs):
        """Select content and returns it in the form of a generator"""
        # searching the tags
        self.search_tag(tag, kwargs)
        # calling the scraper
        return self.scraper.launch_scraper(self.__tags)
