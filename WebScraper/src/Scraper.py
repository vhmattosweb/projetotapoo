import abc


class Scraper(abc.ABC):
    """Abstract class to scrape the data from HTML contet"""
    @abc.abstractmethod
    def launch_scraper(self, tags: list):
        """
        Scrape the data from a list of tags and returns a generator over the results found.

        :param tags: list of tags that we must scrape the data.
        """
        pass


class AthleteTableScraper(Scraper):
    """Scrapes data from the table with results. Example: 100m from rio-2016"""

    # name of the div which we want to scrape the data
    athletes_div_name = "SingleAthleteResultRowGrid"
    # features we should search for and their respective tags and attributes
    features = {
        "Athlete": ("h3", {"data-cy": "athlete-name"}),
        "Country": ("span", {"class": ["styles__CountryName-sc-1r5phm6-1", "eQULfE"]}),
        "Result": ("span", {"data-cy": "result-info-content"})
    }

    def launch_scraper(self, tags: list):
        # loop over the rows of the table
        for row in tags:
            # creating the dictionary with the features we need to search for
            row_features = {}
            # searching for the features
            for feature in self.features:
                try:
                    current_feature = row.find(self.features[feature][0], self.features[feature][1]).string
                except AttributeError:
                    print("Feature %s not found" % feature)
                    current_feature = ""
                row_features[feature] = current_feature
            yield row_features


class LinkScraper(Scraper):
    """Class to scrape links"""
    def launch_scraper(self, tags: list):
        # return the links
        for link in tags:
            if link.string == "See full results":
                yield link.get("href")
