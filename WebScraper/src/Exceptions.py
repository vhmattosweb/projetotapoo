class PageNotFound(Exception):
    """Execption when the page is not found"""
    pass


class TagNotFound(Exception):
    """Execption when the html tag is not found"""
    pass


class MultipleTagsFound(Exception):
    """Exeception when more than one tag is found"""
    pass
