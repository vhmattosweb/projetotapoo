import json
from WebScraper.src.WebPage import WebPage
from WebScraper.src import Selector
from WebScraper.src import Scraper


class OlympicSite:
    """
    Class to represent the Olympic Games website.
    The OlympicSite is a Singleton, since only one instance of this class is needed to scrape all data.
    """
    # variable to store the inscance
    __instance = None
    # Internal api address from the OlympicGames website
    internal_api = "https://olympics.com/en/api/v1/b2p/menu/topbar/olympic-games"
    # list of sports we need to scrape the data from
    sports = ["athletics", "swimming"]

    def __new__(cls):
        """This is required to ensure we are implementing a Singleton"""
        if cls.__instance is None:
            cls.__instance = super(OlympicSite, cls).__new__(cls)
        return cls.__instance

    def __init__(self):
        self.olympic_data = {}

    @classmethod
    def get_editions(cls, edition_type: str = "summer"):
        """
        Read all the Olympic Games editions and return a generator over them.
        Example: Rio-2016, Tokyo-2020, etc.

        Here, we read all editions from a .json file that can be access by the url in internal_api
        variable.

        :param edition_type: It can be either summer or winter.
        :return: generator to loop over all editions
        """
        # creating the WebPage object for the Olympic-API to scrape all editions
        # The Selector must be the JsonSelector the editions are in a json file
        site_with_all_editions = WebPage(cls.internal_api, Selector.JsonSelector())
        # return all editions
        return site_with_all_editions.get_content(edition_type)

    @staticmethod
    def get_marks(url: str):
        """
        Get all Marks available at url.
        The url must contain a Table with the results from a given modality in a specific edition.

        Example of an url: https://olympics.com/en/olympic-games/london-2012/results/athletics/100m-men

        :param url: site address we need to scrape the data
        :return: generator over all the data in the form of a Python dictionary.
                 Ex: {'Athlete': 'usain bold', 'Country': '', 'Result': ''}.
        """
        current_url = "https://olympics.com/" + url
        # creating the TagSelector object
        selector = Selector.TagSelector()
        # adding the table scraper (each TagSelector object must have a Scraper object)
        selector.scraper = Scraper.AthleteTableScraper()
        # loading web page
        current_site = WebPage(current_url, selector)
        # return content
        return current_site.get_content("div", **{"data-cy": "single-athlete-result-row"})

    @classmethod
    def get_modalities_by_edition_by_sport(cls, edition: str, sport: str):
        """
        Get all modalities from a sport of a given edtion and returns a generator over them.

        :param edition: edition we want to get the modalities
        :param sport: sport which we want to get the modality
        :return: generator over all modalities.
        """
        # url for the web page
        url_sport = f"https://olympics.com/en/olympic-games/{edition}/results/{sport}/"
        # creating the selector
        selector = Selector.TagSelector()
        # setting the scraper
        selector.scraper = Scraper.LinkScraper()
        # creating the web page with all
        modalities = WebPage(url_sport, selector)
        # return the generator
        return modalities.get_content("a", **{"data-cy": "next-link"})

    def scrape_all_data(self):
        """Scrape all the data and stores it into a python dictionary."""
        # loop over all editions
        for edition in self.get_editions():
            print("INFO: Scraping edition %s" % edition)
            # initializing the edition in the dictionary
            self.olympic_data[edition] = {}
            # loop over sports
            for sport in self.sports:
                self.olympic_data[edition][sport] = {}
                # loop over all modalities
                for link in self.get_modalities_by_edition_by_sport(edition, sport):
                    # getting all the marks
                    self.olympic_data[edition][sport][link.split("/")[-1]] = [mark for mark in self.get_marks(link)]

    def save_json(self, filename: str):
        """Save the results from the scraper into a .json file"""
        with open(filename, "w") as file_:
            file_.write(json.dumps(self.olympic_data, indent=4))
