from WebScraper.src.Selector import Selector
from WebScraper.src.HTML_Loader import HTML_Loader


class WebPage:
    """
    Class to represent a web page. It must have two attributes:
        1. Selector object, so we can know what to scrape from the given url
    """

    def __init__(self, url: str, selector: Selector):
        # saving the selector object
        self.selector: Selector = selector
        # sets html_loader into selector
        self.selector.html = HTML_Loader(url)

    def get_content(self, tag, **kwargs):
        """
        Returns the content we searched for in the form of a generator.

        :param tag: it can be used to define the tag we are searching for, for example
        :param kwargs: it can be used to define the attributes of the tag we are searching for, for example
        :return: generator over the content found
        """
        # return the content
        return self.selector.select_content(tag, **kwargs)
