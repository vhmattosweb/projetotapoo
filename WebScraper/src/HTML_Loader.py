import requests
from WebScraper.src import Exceptions


class HTML_Loader:
    """Class to read the HTML content."""

    def __init__(self, url: str):
        """Reads the html content of the website located at url"""
        self.__url = url
        # attr to store the result from the request
        self.__request = None
        # making the request
        self.make_request()

    def make_request(self):
        """Make the request to the given website"""
        try:
            # making the request
            self.__request = requests.get(
                self.__url,
                headers={"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 "
                                       "(KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36"},
            )
            # checking if the page exists
            if self.__request.status_code == 404:
                raise Exceptions.PageNotFound()

        except Exceptions.PageNotFound:
            print("Webpage %s not found " % self.__url)
        except:
            print("Not possible to load %s" % self.__url)

    def get_request_result(self):
        """Returns the request object"""
        return self.__request
