"""
Example on how to scrape all modalities of a given edition and sport.
"""
from WebScraper.src.OlympicSite import OlympicSite

if __name__ == "__main__":
    # initializing OlympicSite obj
    olympic_site = OlympicSite()
    # reading marks by path
    for modality in olympic_site.get_modalities_by_edition_by_sport("rio-2016", "swimming"):
        print("Modality link: %s" % modality)
