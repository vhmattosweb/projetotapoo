"""
Example on how to scrape the editions.
"""
from WebScraper.src.OlympicSite import OlympicSite

if __name__ == "__main__":
    # initializing OlympicSite obj
    olympic_site = OlympicSite()
    # reading marks by path
    for edition in olympic_site.get_editions("summer"):
        print(edition)
