"""
File to run all the data scraping.
"""
from WebScraper.src.OlympicSite import OlympicSite

if __name__ == "__main__":
    # initializing OlympicSite obj
    olympic_site = OlympicSite()
    # launch the scraper
    olympic_site.scrape_all_data()
    olympic_site.save_json("ALL_Olympic_Games_Results.json")
