"""
Example on how to scrape data from individual events.

Example: 100m freestyle

Relay events must NOT be scrape using this code.
"""
from WebScraper.src.OlympicSite import OlympicSite

if __name__ == "__main__":
    # initializing OlympicSite obj
    olympic_site = OlympicSite()
    # reading marks by path
    for mark in olympic_site.get_marks("en/olympic-games/tokyo-2020/results/athletics/men-s-800m"):
        print(mark)
