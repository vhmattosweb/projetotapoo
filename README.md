# Olympic Ranking 
### Swimming and Athletics edition

## Descrição Geral

O nosso projeto tem como objetivo proporcionar ao usuário diversas formas de criar rankings com base nos dados olímpicos de duas modalidades individuais: atletismo e natação. 
Para trazer essas informações, rasparemos os resultados do próprio site olímpico (https://olympics.com/en/olympic-games/olympic-results). 
Antes de entrarmos em detalhes sobre os tipos de filtros aos quais o usuário terá acesso, vamos descrever as duas maneiras pelas quais ele poderá visualizar os resultados das suas pesquisas.

A primeira opção consiste em um ranking de atletas. 
Esta alternativa visa ordenar os atletas usando apenas a sua melhor marca na prova em questão. Imagine, por exemplo, que o usuário deseje criar um ranking de atletas para a prova de 50 metros livre na natação. 
Algumas das melhores marcas da história (em piscina longa, incluindo eventos fora das Olimpíadas) são as seguintes:

- César Cielo (Brasil) - 20.91 segundos (2009).
- César Filho (Brasil) - 21.02 segundos (2009).
- Ben Proud (Reino Unido) - 21.11 segundos (2018).
- Florent Manaudou (França) - 21.19 segundos (2014).
- Cameron McEvoy (Austrália) - 47.04 segundos (2016).

Dado que o usuário busca criar um ranking de atletas, consideraremos apenas a marca de 20.91 segundos de César Cielo, excluindo marcas superiores, como 21.11 segundos.

A segunda opção consiste em um ranking de marcas, onde um mesmo atleta pode aparecer várias vezes se tiver mais de uma marca na prova em questão. Neste caso, incluiremos todas as marcas de Cesar Cielo na prova de 100 metros livre, por exemplo.

Além disso, o nosso site proporcionará ao usuário o acesso a diferentes filtros que não são feitos por demais sites que trazem essas informações. Eles são:

- ###### _Filtro por país ou países_:
  
  Selecionaremos somente os resultados dos atletas de um determinado país ou conjunto de países. Por exemplo, se o usuário estiver interessado nas melhores marcas de atletas brasileiros na prova de 100 metros rasos do atletismo ou nas melhores marcas de atletas sul-americanos nos 100 metros rasos.

- ###### _Filtro por edição ou edições_:
  
  Filtraremos somente os resultados de uma ou mais edições específicas. Por exemplo, se o usuário desejar criar um ranking considerando apenas os resultados das edições de Tóquio 2020 e Rio 2016.


- ###### _Filtro por edições e países_:

    Uma combinação dos dois casos acima.

À medida que nos aproximamos do ano olímpico, esperamos que o nosso projeto possa ser uma ferramenta útil para o usuário, especialmente para assessorias esportivas, profissionais da área e entusiastas de esportes que desejam acessar esses dados de maneira simples e rápida.

## Cronograma preliminar

A seguir, detalhamos o planejamento preliminar de entregas para cada uma das fases.

#### Fase 1:

- Implementação da raspagem dos dados do site das olimpíadas;
- Armazenamento dos dados raspados em uma base de dados;
- Implementação de todas as classes necessárias para o armazenamento dos dados rapasdos (não é a implementação das classes responsáveis para ordenar os dados);
- Versão inicial do ranking já com os filtros implementados (classes responsáveis pela ordenação das marcas);
- Implementação de testes para o código desenvolvido (ou pelo menos para parte do código desenvolvido).

OBS: Nesta entrega ainda não faremos uma interface Web.

#### Fase 2:

- Implementação da interface web, proporcionando ao usuário a possibilidade de filtrar os dados por país ou por edições;
- Implementação de pelo menos 1 padrão de projeto GoF, que ainda será decidido pelo grupo;
- Implementação da REST API para processor os filtros selecionados do usuário e listar os dados que satisfazem os mesmos para o front-end;
- Implementação de testes para o código desenvolvido.

#### Fase 3:

- Sistema Web implementado já com as correções de possíveis bugs no código;
- Teste manual para garantir que as funcionalidades exigidas do site foram atendidas;
- Implementação de 4 padrões de projeto GoF/arquiteturais, que ainda serão decididos pelo grupo;
- Implementação de testes para as partes de código que não foram cobertas pelos testes anteriores.

## Entrega 1 - 10/10/2023

Na primeira fase do projeto, desenvolvemos dois programas essenciais para a construção do nosso sistema. 
O primeiro programa envolveu a implementação da extração de dados das marcas das modalidades de natação e atletismo 
de todas as edições das Olimpíadas. Os dados foram obtidos a 
partir do seguinte site:  
[https://olympics.com/en/olympic-games/olympic-results](https://olympics.com/en/olympic-games/olympic-results).

O segundo programa concentrou-se no desenvolvimento das classes principais do nosso sistema. 
Com essas classes em funcionamento, já conseguimos criar rankings de diferentes modalidades, 
permitindo a aplicação de diversos filtros que poderão ser escolhidos pelos usuários. 
A seguir, discutimos em mais detalhes o que foi realizado em cada uma dessas implementações.

#### Implementação da Raspagem de Dados

Os códigos desenvolvidos para a implementação da raspagem de dados podem ser encontrados no diretório [WebScraper/src](WebScraper/src). 
Além disso, scripts de exemplo para a execução da raspagem estão disponíveis no diretório [Webscraper/examples](WebScraper/examples). 
Por exemplo, ao executar o arquivo [scraper.py](WebScraper/examples/scraper.py), todos os dados da página, incluindo edições, modalidades, marcas, 
entre outros, são raspados e salvos em um arquivo no formato JSON. 

Para implementar o sistema de raspagem de dados, criamos abstrações que tornaram o programa independente da página 
específica que estamos extraindo. A única parte do sistema que depende do conhecimento específico das tags HTML é a 
classe 'Scraper'. No entanto, essa classe é abstrata, e suas implementações lidam com os detalhes específicos 
dos dados a serem extraídos. Por exemplo, a classe 'AthleteTableScraper' sabe como identificar nomes de atletas, 
seus países e suas marcas em uma página que contenha essas informações. No entanto, extraímos esse comportamento
específico das classes principais usando o padrão Strategy. Além disso, criamos uma classe responsável pela extração 
de dados de arquivos JSON, pois notamos que parte das informações necessárias estava presente em arquivos desse tipo. 
Por fim, desenvolvemos a classe 'OlympicSite', que contém métodos para executar a raspagem de páginas específicas.

Na pasta [WebScraper.pdf](WebScraper/docs/UML_Diagram.pdf) colocamos um arquivo pdf com o diagrama UML desse sistema.

#### Persistência de Dados em um Banco de Dados MySQL

Os dados raspados foram persistidos em um banco de dados MySQL, que foi implantado em uma máquina virtual na 
Oracle Cloud para fins desta disciplina. A estrutura de tabelas criada no banco de dados pode ser verificada 
no arquivo [create-tables.sql](database/scripts/create-tables.sql). 
Portanto, após a raspagem inicial dos dados, a fonte de dados da aplicação será exclusivamente o banco de dados.

Para a inserção dos dados no banco, utilizamos o padrão Adapter que pode ser visto através do nosso diagrama UML 
([Adapter.pdf](database/docs/Database.pdf)). 

#### Implementação das Classes Core

Durante esta fase, implementamos as classes centrais do projeto, incluindo Atleta, Evento, Modalidade, Esporte e Edição,
que são classes que imitam a estrutura e associações presentes no banco de dados, a qual será preservada pelas
associações entre os objetos dessas classes. 
A implementação de todas essas classes pode ser encontrada em [src/core_classes](src/core_classes).
Para instanciar esses objetos, desenvolvemos uma classe chamada 'OlympicDataBuilder', 
que lê todas as informações necessárias para a criação deles a partir de um objeto denominado 'FrozenJSON'.
Este objeto possui uma estrutura semelhante à de um dicionário em Python. 
Vale ressaltar que os atributos dos objetos são criados dinamicamente. Em nosso sistema, 
isso significa que somente os atributos que foram extraídos do banco de dados são criados. 
Por exemplo: se extraimos somente o nome do atleta, o atributo que fará referência ao seu país de origem não é criado.
Note que em nenhuma dessas classes foi utilizado exceções (exceptions) para lidar com possíveis erros de busca de um atributo que não
foi definido durante a criação do objeto. Reconhecemos que essa abordagem pode causar problemas e planejamos corrigir 
isso nas próximas entregas do projeto.  

Além da criação das classes core, criamos classes para a requisição de dados. A classe mais importante se chama 
OlympicDataRequest. Essa classe estabelece conexão com o banco de dados por meio de um objeto da classe "Database" e 
seleciona os dados utilizando a query definida pelo objeto "SelectQuery". 
O SelectQuery, por sua vez, guarda uma referência para um objeto da classe "WhereClause". Esta última 
classe é responsável por definir as especificações que um dado deve atender para ser selecionado. 
Esta classe tem uma atuação importante no nosso sistema, pois a classe "Filter", que representa os filtros que são 
escolhidos pelo usuário, cria um objeto do tipo "WhereClause" com as especifícações selecionadas pelo mesmo. 
As classes mencionadas aqui podem ser encontradas no diretório [database/src](database/src), enquanto a classe
Filter pode ser encontrada no diretório [src/core_classes](src/core_classes).

As classes para a elaboração dos rankings também foram criadas e podem ser encontradas no diretório [src/](src/). 
O nosso sistema visa a construções de dois tipos de rankings: marcas e atletas. A construção do segundo 
foi feito adicionando uma classes decoradora que envelopa o ranking de marcas.

Para obter mais detalhes sobre as outras classes implementadas, que não foram discutidas aqui, 
recomendamos consultar o diagrama UML do nosso sistema. O diagrama pode ser acessado pelo link [docs/UML.pdf](docs/UML.pdf).

Alguns modelos de rankings que poderão ser criados pelos usuários estão disponíveis na pasta [examples/](examples).
Instruções de como executar esse arquivo estão logo abaixo.

#### Testes

Foram desenvolvidos testes para as classes core, que podem ser encontrados em [src/tests](src/tests). 

#### Padrões de Projeto

Nessa primeira fase, implementamos alguns padrões de projeto, sendo eles:

- Decorator: a classe 'RankByAthletes' é uma implementação concreta da classe abstrata 'RankDecorator' que envelopa 
um objeto do tipo Ranking. A classe 'RankingByAthletes' remove marcas em que o atleta é o mesmo, 
mantendo somente a melhor marca de um único atleta;
- Composite, na implementação das classes responsáveis pelos filtros e também na implementação das classes responsáveis
pela 'WhereClause'. Com essa implementação as classes clientes (classes que utilizam essas interfaces) não precisam 
fornecer um tratamento diferenciado caso tenhamos mais de um filtro ou mais de uma 'where clause';
- Strategy: utilizamos esse padrão na implementação do nosso sistema de raspagem de dados como discutido anteriormente;
- Adapter: para a inserção dos dados raspados no banco de dados.


#### Executando os arquivos de exemplo

Para executar os arquivos de exemplo mencionados acima, é necessário ter as dependências listadas no arquivo [requirements.txt](requirements.txt) instaladas.
Depois, para executar um arquivo, escreva o seguinte comando:

``
python -m examples.example_1 # executa o arquivo examples/example_1.py
``
#### Próxima fase

Para a próxima etapa do projeto (Fase 2), temos uma série de tarefas planejadas para aprimorar nosso sistema:

1 - Aprimoramento da Raspagem de Dados: Vamos explorar a busca de um site alternativo para a coleta de dados, já que o site das Olimpíadas não fornece todas as informações necessárias. Isso garantirá que tenhamos acesso a todos os dados relevantes.

2 - Tratamento de Exceções: Abordaremos minuciosamente todas as exceções nas classes que simulam nosso banco de dados. Isso contribuirá para um sistema mais robusto e à prova de erros.

3 - Melhoria das Unidades de Marca: Nas classes "ranking", realizaremos a conversão das marcas para unidades mais compreensíveis aos usuários. Isso é essencial, pois todas as marcas em nosso banco de dados estão atualmente em segundos.

4 - Desenvolvimento da API (REST): Trabalharemos na criação de endpoints para nossa API, utilizando o framework Flask em Python. Essa etapa é fundamental para disponibilizar os dados coletados e processados de maneira eficaz e acessível aos usuários e outras aplicações.

5 - Testes Abrangentes: Por fim, conduziremos testes abrangentes para garantir que todas as funcionalidades estejam operando corretamente. Isso incluirá testes de unidade, testes de integração e testes de aceitação, a fim de assegurar a qualidade e o desempenho de nosso desenvolvimento.

6 - Protótipo Interface Web: Iremos criar um protótipo que representará nosso front-end que será desenvolvido na próxima entrega (Fase 3)

Com essas medidas, buscamos aprimorar significativamente nosso projeto e garantir que ele atenda aos requisitos de qualidade, desempenho e usabilidade.


Na Fase 3, elaboramos uma lista de atividades que é uma continuação da Fase 2. Portanto, a lista inclui as atividades planejadas a serem realizadas no caso de uma conclusão bem-sucedida da Fase 2.

- Desenvolvimento do Front-end com base no protótipo.
- Realização de testes.

A seguir, apresentamos o diagrama UML da aplicação até o momento:

![Diagrama UML](docs/UML.pdf)

## Entrega 2 - 07/11/2023

Nesta segunda fase do projeto, nossos principais avanços foram os seguintes:

1. Conclusão da raspagem de dados que necessitávamos;
2. Ajustes nas classes centrais do nosso sistema que não foram feitos na entrega anterior;
3. Implementação de uma API REST com os métodos GET e POST para a aquisição de dados;
4. Criação de um protótipo da nossa interface web com as funcionalidades às quais o
usuário terá acesso.

A seguir, detalhamos o que foi realizado em cada uma das áreas mencionadas acima. 

#### Finalização da raspagem de dados

Na fase anterior, observamos que o site das Olimpíadas não disponibilizava marcas para a maioria das 
modalidades de natação. Essas marcas são de extrema importância para nós, uma vez que nosso site tem como objetivo 
criar um ranking das modalidades de natação e atletismo.

Devido a isso, tivemos que coletar as marcas das modalidades de natação de outro site. O site escolhido foi o 
[WorldAquatics](https://www.worldaquatics.com/), anteriormente conhecido como FINA (Federação Internacional de Natação). 
Nele, encontramos as marcas de todas as modalidades de natação de campeonatos oficiais, incluindo as competições olímpicas.

A extração de dados desse site foi mais trabalhosa, uma vez que tivemos que explorá-lo extensivamente para descobrir 
onde deveríamos extrair as informações necessárias. Em resumo, adotamos a seguinte estratégia:

1. Obtivemos os links referentes às edições passadas dos Jogos Olímpicos por meio desta página:
[www.worldaquatics.com/competitions/2943/olympic-games-paris-2024](https://www.worldaquatics.com/competitions/2943/olympic-games-paris-2024). 
Observe que no final da página, há links para todas as edições abaixo de 'Other Editions'.
2. Com o link de uma edição específica, conseguimos acessar a página que contém todas as modalidades que ocorreram nela.
Por exemplo, você pode ver as modalidades que ocorreram nos Jogos do Rio de Janeiro em 2016 [aqui](https://www.worldaquatics.com/competitions/262/olympic-games-rio-2016/results?disciplines=SW). 
Cada modalidade possui um ID denominado "eventguid," que será utilizado...
3. ...para obter todas as marcas da modalidade da edição em questão por meio de uma API interna do 
site [https://api.worldaquatics.com/fina/events/event-guid](https://api.worldaquatics.com/fina/events/4d0b20af-20fa-403b-addd-fd15967881c6),
em que "event-guid" deve ser substituído pelo valor que coletamos no item anterior.

Para essa nova raspagem, optamos por não usar o programa que desenvolvemos na entrega anterior, por um motivo especial: 
desejávamos refatorar o código que criamos, aplicando um padrão de projeto que não havia sido utilizado anteriormente. 
O padrão que adotamos agora foi o Chain of Responsibility.

As classes que desenvolvemos estão localizadas no diretório WebScraper_v3. 
A classe principal desta implementação é a PageScraper, e é com ela que aplicamos o padrão Chain of Responsibility. 
A ideia por trás disso é a seguinte: a raspagem das marcas de uma edição dos Jogos Olímpicos geralmente envolve 
a raspagem de outras páginas que contêm informações relacionadas a essa edição. 
Ou seja, podemos criar uma classe que é capaz de coletar os links das páginas com informações sobre as edições, 
e a raspagem do conteúdo em cada uma dessas páginas pode ser delegada a outra classe criada para esse fim. 
Se todas essas classes seguirem a mesma interface, temos o padrão Chain of Responsibility. 
Utilizamos essa abordagem tanto para coletar informações sobre as modalidades em uma edição quanto para coletar 
as marcas de uma modalidade em uma edição.

O diagrama UML desse sistema pode ser encontrado em [WebScraper_v3/docs](WebScraper_v3/docs) para mais informações sobre as outras 
classes usadas nessa implementação. Note que aqui também usamos o padrão Strategy de uma forma semelhante à 
que utilizamos no programa anterior. Cada implementação concreta da interface Scraper contém o algoritmo de raspagem 
que deve ser utilizado para coletar os dados de uma dada página.

#### Ajustes nas classes core do sistema

###### 1. Tratamento de exceções 

Na entrega anterior, mencionamos que a criação dos atributos das classes, que imitavam a estrutura do banco de dados,
é feita dinamicamente. Isto é, apenas criamos atributos que são requisitados do banco de dados.
Isso pode levar a um bug no nosso sistema se alguma classe que interage com elas tentar acessar 
um atributo que não foi criado.

Para contornar isso, adicionamos um decorador em cada um dos métodos de acesso dos atributos das classes core que fará o tratamento de exceção caso o atributo não tenha sido criado.
O método que lida com o tratamento de exceções está definido na classe BaseObject, que pode ser encontrada 
em [src/core_classes](src/core_classes), juntamente com as classes core que utilizam esse decorador.

Dessa maneira, evitamos a repetição de código em cada um dos métodos de acesso,
uma vez que o tratamento seria igual para todos.

Uma boa explicação sobre decoradores em Python e como usá-los pode ser encontrada no Capitúlo 9 do livro 
Fluent Python, de Luciano Ramalho [(link)](https://www.amazon.com/Fluent-Python-Concise-Effective-Programming/dp/1491946008).

###### 2. Melhorias nas unidades das marcas

Para padronizar o armazenamento das marcas no banco de dados, convertemos todas aquelas que possuem uma unidade de tempo em segundos.
Assim, as marcas com unidades de tempo e distância puderam ser armazenadas como floats. 

Para apresentar as marcas de tempo aos usuários, convertemos novamente para a unidade de tempo, 
exibindo-as no formato hora:minuto:segundo.
A conversão foi feita dentro da classe Mark, que pode ser encontrada na pasta [src/core_classes](src/core_classes).

###### 3. Conversão das objetos instanciados para dicionários

Para enviar o resultado da criação das marcas de um ranking para o front-end, precisamos converter os objetos 
que representam as marcas em um dicionário, que será utilizado pela API para renderizar a tabela com as marcas 
para o usuário. Essa conversão foi feita pela classe JsonCreator ([src/JsonCreator.py](src/JsonCreator.py)),
a qual converte os objetos e todas as suas referências em um dicionário.

Por exemplo, dado uma instância de Mark, ele criará um dicionário cujas chaves são representadas 
pelos nomes dos métodos de acesso e seus valores correspondem ao valores retornados por estes métodos.

Essa classe utiliza recursão. Caso o objeto em questão faz referência para outro objeto da mesma ou outra classe, este
também é convertido para um dicionário.

###### 4. Ordenação das marcas das provas de campo do atletismo

As modalidades do atletismo são separadas em duas categorias: modalidades de pista e modalidades de campo. 
A primeira se refere a todas as provas de corrida (100m rasos, 10.000m, maratona, etc). Neste caso, as marcas estão em 
unidades de tempo e a melhor marca é sempre a que possui o menor tempo. Isso implica que a ordenação do ranking deve ser
da menor para maior marca e devemos converter todas elas, que estão em segundos, para hora:minuto:segundo, se 
necessário. 

Agora, as provas de campo são as provas de lançamentos, arremessos e saltos. Neste caso, a melhor marca é sempre aquela
que atingiu a maior distância. Isso nos diz que um ranking dessas provas deve ordenar as marcas da maior 
para a menor e não devemos converter estas para hora:minuto:segundo. 

No nosso banco de dados não salvamos um identifador para distinguir as modalidades de campo e modalidades de pista. 
Por conta disso, tivemos que lidar com isso no back-end do nosso site. 

Para resolver esse impasse, criamos uma nova classe chamada de [ModalityObserver](src/ModalityObserver.py), cujo objetivo
é manter o controle de qual é a modalidade que está sendo requisitada para montar o ranking. Esta classe é um Singleton. 
Todas as classes que a acessam, compartilham a informação de qual é a unidade da modalidade que está sendo pedida. 

Esta classe possue dois métodos: um para atualizar a modalidade atual e um para informar se a unidade das marcas é de 
distância ou não. O primeiro é acionado sempre quando um filtro de modalidade é criado, assim sabemos qual é a modalidade que 
estamos lidando. O segundo é acionado pelo Ranking e pelas instâncias de Mark para saberem como devem ordenar e exibir as marcas, respectivamente. 

Um simples diagrama UML para ilustrar o seu uso pode ser encontrado em [docs/](docs).

#### Desenvolvimento da API REST
Nesta fase foi desenvolvida a API responsável por disponibilizar os dados ao usuário, através de requisições HTTP, com o auxílio do framework [Flask](https://flask-ptbr.readthedocs.io/en/latest/) para gerenciamento das requisições e rotas da aplicação.

Os endpoints disponíveis são apresentados a seguir:

**1. /ranking** [GET]
  O método GET deste endpoint retorna a página HTML inicial da aplicação, com os filtros default previamente selecionados.


**2. /ranking** [POST]
  Ja o método POST deste endpoint é chamado assim que o usuário faz a sua busca na interface. Ele é responsável por receber um formulário, com campos contendo os filtros desejados, enviar essa informação para processamento no back-end e retornar o resultado renderizado numa página HTML.

  No corpo da requisição POST espera-se um Form com os seguintes parâmetros:

  * sport (obrigatório): id do esporte 
  * modality (obrigatório): id da modalidade
  * edition: ids das edições
  * country: ids dos países
  * view: maneira de visualização do ranking (athletes para ranking de atletas, default para ranking de marcas)

  Abaixo seguem dois exemplos de requisição a este endpoint utilizando CURL:
  
  * Ranking de marcas da modalidade *Men's 1000m* do Atletismo, de todos os países e todas as edições de Olimpíadas:
  ```shell
    curl --request POST \
  --url http://127.0.0.1:5000/ranking \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data sport=22 \
  --data modality=411
  ```

* Ranking de atletas dos Estados Unidos, na modalidade *Women's Medley 400m* da Natação, das edições Tokio 2020, Rio 2016 e Londres 2012:
```shell
  curl --request POST \
    --url http://127.0.0.1:5000/ranking \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data sport=23 \
    --data modality=389 \
    --data edition=134 \
    --data edition=135 \
    --data edition=136 \
    --data country=409
```


**3. /modality** [GET]: 

Retorna todas as modalidades de um esporte. Na *query* deve ser passado o parâmetro *sport* contendo o id do esporte desejado (atletismo=22, natação=23). Abaixo segue um exemplo de requisição utilizando CURL que retorna todas as modalidades do atletismo:

```shell
curl --request GET --url 'http://127.0.0.1:5000/modality?sport=22'
```

Esse endpoint é chamado internamente com a finalidade de popular dinamicamente o filtro de modalidades com base na opção de esportes selecionada pelo usuário. Isso foi possível utilizando JavaScript para a manipulação de elementos HTML e seus eventos relacionados.



#### Protótipo da Interface Web

Nesta fase foi inicializado o desenvolvimento da interface web, priorizando o funcionamento da comunicação com a API e a boa visualização das informações. Para isso, escolhemos um design simples, porém amigável, que será refinado na próxima fase. Para auxiliar no desenvolvimento do front-end utilizamos o *framework* [Bootstrap](https://getbootstrap.com/).

O design da aplicação consiste em uma página com um grupo de seleção de filtros no topo e uma tabela em destaque com os dados do ranking (posição, nome do atleta, país, edição e marca). Ao acessar a página inicial, os filtros obrigatórios de esporte, modalidade e tipo de ranking são pré selecionados. Edição e país vêm sem seleção por padrão.

Ambos os filtros de edição e país aceitam zero ou mais opções. Para possibilitar esse comportamento na interface, foi utilizado o atributo de *multiselect* do HTML. A intenção para a próxima fase é melhorar este elemento para algo de manuseio mais fácil e intuitivo, e que forneça mais informação a respeito das opções já selecionadas.

> Utilização:
Para utilizar a interface, comece selecionando os filtros de **Esporte**, e então **Modalidade**. Depois disso, selecione as opções desejadas de **Edição** e **País**. Para selecionar/deselecionar uma opção, dê *ctrl+clique* na opção desejada. Por último, selecione se deseja visualizar o ranking de atletas ou de marcas, e clique no botão **Search** para realizar a sua busca.





#### Padrões de Projeto

Nesta entrega utilizamos três padrões de projeto, que se tornaram importantes para a nossa implementação. Eles foram:

1. Chain of responsability, usado na implementação da nova versão de raspagem;
2. Strategy, também utilizado na nova versão da raspagem;
3. Singleton, usado no back-end do nosso projeto para armazenar a modalidade sendo usada na construção do ranking.

#### Execução do código desenvolvido
Para executar a API, vá até a pasta 'api' e rode o seguinte comando:
```shell
  flask run
```

Você poderá acessar a aplicação através da seguinte url:

http://127.0.0.1:5000/ranking


#### Rankings sugeridos

Para fins de ilustração, sugerimos os seguintes rankings:

1. Ranking de atletas dos 800m do atletismo, aplicando o filtro de país, selecionando apenas o BRA.

Neste ponto você deve ver marcas de atletas brasileiros nesta prova, com Joaquim Cruz tendo a melhor marca entre os brasileiros. Note que todas foram antes dos anos 90.

2. Ranking de marcas dos 800m do atletismo, aplicando o filtro de edição, selecionando apenas a edição de Los Angeles 1984.

Aqui você perceberá que Joaquim Cruz não tem somente a melhor marca dos 800m entre os brasileiros, como também ganhou ouro na edição que fez sua melhor marca nos Jogos Olímpicos.

3. Ranking de marcas dos 800m do atletismo, aplicando o filtro de edição, selecionando todas edições anteriores a Los Angeles 1984, incluindo esta. 

Joaquim Cruz não somente fez história trazendo o ouro olímpico para o Brasil, mas também bateu o recorde olímpico em 1984. 

#### O que não entregamos nesta fase

Infelizmente, nesta entrega, não conseguimos desenvolver testes automatizados, aos quais nos comprometemos a entregar. 
A realização dos testes automatizados ficou a cargo de um ex-integrante do grupo, que nos avisou que não fará mais parte 
do projeto devido a problemas pessoais. Como fomos avisados na última semana, não pudemos dividir essa tarefa entre os 
membros presentes, e vamos incluir a cobertura de testes automatizados na próxima entrega.

#### Planejamento para última fase do projeto

Para a próxima fase do projeto, vamos focar nas seguintes tarefas:

1. Desenvolver testes automatizados para o nosso sistema desenvolvido até o momento;
2. Aperfeiçoar a interface do site que desenvolvemos. Focando em melhorar a usuabilidade na escolha de filtros.


## Entrega 3 - 21/12/2023

Nesta última entrega trabalhamos em três frentes:

1. Aumentamos a nossa bateria de testes automatizados;
2. Aprimoramos a interface Web do sistema;
3. Adicionamos um novo funcionamento no sistema.

Abaixo detalhamos o que foi feito brevemente em cada uma delas.

### Testes automatizados

Aumentamos a cobertura de testes no nosso sistema, principalmente para as classes que são essênciais para o funcionamento do 
site. As classes testadas foram:

*  SingleTable e JoinTable;
*  WhereClause;
* SelectQuery;
* OlympicDataRequest;
* FrozenJSON;
* OlympicDataBuilder;
* OlympicData;
* Filter;
* FilteredRaking.

Estes testes podem ser encontradas na pasta [src/tests](src/tests) e [database/src/tests](database/src/tests).
Além disso, testes na API também foram implementados e podem ser encontrados em [api/test_api.py](api/test_api.py).

Para rodá-los, basta utizilizar o seguinte comando no terminal:

```shell
  pytest nome_do_arquivo_com_testes.py
```

### Melhorias na interface do site

Melhorias na interface foram feitas e elas podem ser visualizadas rodando o próprio site da mesma maneira que foi 
explicado na entrega anterior: vá até a pasta da api e digite
```shell
  flask run
```
e clique no link http://127.0.0.1:5000, em seguida corrija a url para
http://127.0.0.1:5000/ranking.

#### Nova funcionalidade

A nova funcionalidade que propomos consiste em visualizar o histórico de marcas do atleta, as quais foram ordenadas 
de acordo com a edição das Olimpíadas (a marca mais antiga para a mais recente). Essa informação pode ser visualizada 
clicando no nome do atleta em algum ranking gerado.

Para adicionar essa nova funcionalidade precisamos refatorar o código possibilitando novas estratégias de ordenação.
A mudança que fizemos no sistema pode ser encontrada no diagrama Refatoracao.pdf na pasta docs/.


## Integrantes

Lorena Braghini Miranda (@lorenabraghini)

Marília Machado Fernandez (@mariliafernandez)

Matheus Martines de A. da Silva (@mathmartines)

Victor Mattos (@vhmattosweb)
