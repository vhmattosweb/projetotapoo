from abc import ABC, abstractmethod
import requests


class WebPageInterface(ABC):
    """Class to store the adress of a given web page"""

    @abstractmethod
    def get_html_content(self) -> str:
        """Returns the html content of the page"""
        pass


class WebScraperInterface(ABC):
    """Interface for the WebScraper classes, """

    @abstractmethod
    def scrape_page(self):
        pass

    @abstractmethod
    def delegate_to_next(self):
        pass


class ScraperInterface(ABC):
    """Interface for the scraper algorithm"""

    @abstractmethod
    def launch_scraper(self):
        pass


