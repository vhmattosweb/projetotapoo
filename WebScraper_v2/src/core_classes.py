from abc import ABC, abstractmethod
import requests
from selenium import webdriver
from WebScraper.src.Exceptions import PageNotFound


class WebPage:
    """Class to store the HTML content of a Web Page"""

    def __init__(self, url: str, request_obj=False):
        # stores the url of the page
        self.__url = url
        if request_obj:
            self.__request = self.json_request(self.__url)
        else:
            # variable to store the html request
            self.__driver = self.make_request(self.__url)

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, url: str):
        # updating the url
        self.__url = url
        # making the request with the new URL
        self.__driver = self.make_request(self.__url)

    @property
    def request(self):
        return self.__request

    @property
    def driver(self):
        return self.__driver

    def close_page(self):
        """Close the connection with the web page"""
        if hasattr(self, "_WebPage__driver"):
            self.__driver.close()

    @staticmethod
    def make_request(url: str):
        """Make the request to the given website"""
        try:
            # accessing the page
            driver = webdriver.Firefox()
            driver.get(url)
            driver.implicitly_wait(20)
            # return content
            return driver
        # catching page not found error
        except:
            print("Webpage %s not found " % url)
        # returns None in case the page was not found
        return None

    @staticmethod
    def json_request(url: str):
        try:
            # making the request
            request = requests.get(
                url,
                headers={"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 "
                                       "(KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36"},
            )
            # checking if the page exists
            if request.status_code == 404:
                raise PageNotFound()

            return request

        except PageNotFound:
            print("Webpage %s not found " % url)

        return None


class Scraper(ABC):
    """Abstract class to scrape data from a given web-page"""
    @classmethod
    @abstractmethod
    def __call__(cls, web_page: WebPage):
        pass
