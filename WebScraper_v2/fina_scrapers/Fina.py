from WebScraper_v2.src.core_classes import WebPage
from WebScraper_v2.fina_scrapers import scrapers
import json


class Fina:

    def __init__(self):
        # all editions
        self.__editions = "https://www.worldaquatics.com/competitions/2943/olympic-games-paris-2024"
        # stored the data
        self.__data = {}

    def scrape_editions(self):
        """Scrape all editions"""
        # web-page that holds the list of editions
        web_page = WebPage(self.__editions, True)
        # returns all editions with the links
        return scrapers.finaEditionScraper()(web_page)

    @staticmethod
    def scrape_modalities(url: str):
        """Scrapes all modalities from a given url"""
        # web-page
        web_page = WebPage(url)
        number_of_tries, all_links = 0, {}
        # runs the scraper
        while number_of_tries < 10:
            all_links = scrapers.finaModalityLinkScraper()(web_page)
            if len(all_links) > 0:
                break
            number_of_tries += 1
        # close web-page
        web_page.close_page()
        # return results
        return all_links

    def launch_scraper(self):
        # loop over all editions
        for edition, link_edition in self.scrape_editions().items():
            print(f"INFO: scraping edition {edition} ({link_edition})")
            # adding edition to data
            self.__data[edition] = {}
            # loop over modalities
            for modality, link_modality in self.scrape_modalities(link_edition).items():
                print(f"INFO: Scraping {modality} ({link_modality})")
                # web page with the results
                web_page = WebPage(link_modality, True)
                # scraping the results
                self.__data[edition][modality] = scrapers.finaTableScraper()(web_page)

    def save_result(self, filename):
        with open(filename, "w") as file_:
            file_.write(json.dumps(self.__data, indent=4))



if __name__ == "__main__":
    fina = Fina()
    fina.launch_scraper()
    fina.save_result("FINA_Results.json")
