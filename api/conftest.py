from api.app import run_app
import pytest 

@pytest.fixture
def app():
    app = run_app()
    return app