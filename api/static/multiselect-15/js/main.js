$(function() {

	$('select[multiple].active.3col').multiselect({
	  columns: 3,
	  placeholder: 'Select options',
	  search: true,
	  searchOptions: {
	      'default': 'Search option'
	  },
	  selectAll: true
	});

	$('select[multiple].active.4col').multiselect({
		columns: 4,
		placeholder: 'Select options',
		search: true,
		searchOptions: {
			'default': 'Search option'
		},
		selectAll: true
	  });

});
