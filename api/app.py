from flask.views import MethodView
from flask import Flask, request, render_template
from src.core_classes.Filter import *
from src.Rankings import FilteredRanking
from src.SortStrategy import SortByBestMark, AthleteBestMark, SortByEdition
from src.OlympicData import OlympicData
from src.JsonCreator import JsonCreator


def run_app():
    app = Flask(__name__)
    app.add_url_rule(f"/ranking", view_func=RankingView.as_view("ranking"))
    app.add_url_rule(f"/modality", view_func=ModalityView.as_view("modality"))
    return app


class RankingView(MethodView):
    """MethodView for ranking the Olympic marks and athletes with given filters."""

    # list to hold the filter classes that we'll use to instantiate the filters
    filter_map = {
        "sport": SportFilter,
        "modality": ModalityFilter,
        "edition": EditionFilter,
        "country": CountryFilter,
    }
    # default filters values
    default_filters = {
        "sport": [22],  # athletics
        "modality": [411],  # men_10000m
        "edition": [],
        "country": [],
    }

    def __init__(self) -> None:
        # list to hold the filter object to apply to the ranking
        self.filters = []
        # dictionary to hold the filters asked by the user. If none is asked, we use the default values
        self.request_filters = self.default_filters
        self.view = "marks"

    @staticmethod
    def populate_filters():
        return {
            "edition": sorted(
                OlympicData().get_genercic_table("EditionData"), reverse=True
            ),
            "country": sorted(OlympicData().get_countries()),
            "sport": OlympicData().get_genercic_table("Sport"),
        }

    def instantiate_filters(self):
        """Instantiate the filter object for each case"""
        for key, values in self.request_filters.items():
            if len(values):
                self.filters.append(self.filter_map[key](values))

    @staticmethod
    def validated():
        """Validates if the request body contains all required parameters"""
        required_params = ["sport", "modality"]
        return all([key in request.form.keys() for key in required_params])

    def get(self):
        return render_template(
            template_name_or_list="index.html",
            context={
                "filters": self.populate_filters(),
                "selected_filters": self.default_filters,
                "view": self.view,
                "case_empty": "Select the filters above and press <Search>",
            },
        )

    def post(self):
        """
        Handle POST method, instantiate filters and returns the results rendered in HTML page.
        """
        if self.validated():
            self.request_filters = self.default_filters
            # getting the selected values from the user
            self.request_filters["sport"] = request.form.get("sport")
            self.request_filters["modality"] = request.form.get("modality")
            self.request_filters["edition"] = request.form.getlist("edition")
            self.request_filters["country"] = request.form.getlist("country")
            self.view = request.form.get("view")

            # creating the filters
            self.instantiate_filters()
            # creating the composite filter object
            composite_filter = CompositeFilter(self.filters)

            sorting_strategy = SortByBestMark()
            if self.view == "athletes":
                sorting_strategy = AthleteBestMark(sorting_strategy)
            ranking = FilteredRanking(composite_filter, sorting_strategy)

            # getting the json data
            data = JsonCreator(ranking.get_marks())

            return render_template(
                template_name_or_list="index.html",
                context={
                    "ranking": data,
                    "filters": self.populate_filters(),
                    "selected_filters": self.request_filters,
                    "view": self.view,
                    "case_empty": "No content to show for the selected filters.",
                },
            )
        return "Required parameters missing from request body.", 400


class ModalityView(MethodView):
    def get(self):
        sport_id = request.args.get("sport")
        records = OlympicData().get_modality_by_sport(sport_id)
        return JsonCreator(sorted(records))


class AthleteView(MethodView):
    def get(self):
        athlete_id = request.args.get("athlete")
        modality_id = request.args.get("modality")

        filters = [ModalityFilter(modality_id), AthleteFilter([athlete_id])]
        composite_filter = CompositeFilter(filters)
        sorting_strategy = SortByEdition()
        ranking = FilteredRanking(composite_filter, sorting_strategy)
        data = JsonCreator(ranking.get_marks())

        athlete = data[0]["athlete"]
        modality = data[0]["event"]["modality"]["name"]
        sport = data[0]["event"]["modality"]["sport"]["name"]
        marks = [item["absolute_value"] for item in data]
        raw_values = [item["value"] for item in data]
        editions = [item["event"]["edition"]["name"] for item in data]

        return render_template(
            template_name_or_list="athlete.html",
            data=data,
            context={
                "athlete": athlete,
                "modality": modality,
                "sport": sport,
                "marks": marks,
                "editions": editions,
                "raw_values":raw_values
            },
        )


app = Flask(__name__)
app.add_url_rule(f"/ranking", view_func=RankingView.as_view("ranking"))
app.add_url_rule(f"/modality", view_func=ModalityView.as_view("modality"))
app.add_url_rule(f"/athlete", view_func=AthleteView.as_view("athlete"))
