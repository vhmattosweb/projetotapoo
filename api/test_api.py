import pytest
from api.app import run_app


class TestRanking:
    @pytest.fixture
    def app(self):
        app = run_app()
        return app

    @pytest.fixture
    def client(self, app):
        return app.test_client()

    def test_index_page(self, client):
        response = client.get('/ranking')
        assert response.status_code == 200

    def test_post_valid_data(self, client):
        data = {
            'sport': '22',
            'modality': '411',
            'edition': ['2020', '2016', '2012'],
            'country': ['USA', 'BRA', 'ESP'],
            'view': 'marks'
        }

        response = client.post('/ranking', data=data)
        assert response.status_code == 200

    def test_post_no_content(self, client):
        data = {
            'sport': '2020',
            'modality': '2020',
            'edition': ['5555'],
            'country': ['AAA'],
            'view': 'marks'
        }

        response = client.post('/ranking', data=data)
        assert response.status_code == 200
        assert b"No content to show for the selected filters" in response.data

    def test_post_missing_modality(self, client):
        incomplete_data = {
            'sport': '22',
            # Missing 'modality' key
            'edition': ['2020'],
            'country': ['USA'],
            'view': 'marks'
        }

        response = client.post('/ranking', data=incomplete_data)
        assert response.status_code == 400

    def test_post_missing_sport(self, client):
        incomplete_data = {
            # Missing 'sport' key
            'sport': '411',
            'edition': ['2020'],
            'country': ['USA'],
            'view': 'marks'
        }

        response = client.post('/ranking', data=incomplete_data)
        assert response.status_code == 400

