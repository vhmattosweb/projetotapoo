"""
Example 1.

Ranking of male 50m freestyle marks, considering only the following editions: Tokyo 2020, Rio 2016, and London 2012.
"""

from src.core_classes import Filter
from src.Rankings import FilteredRanking
from src.SortStrategy import SortByBestMark

if __name__ == "__main__":
    # selecting the modality we will construct the raking
    modality = Filter.ModalityFilter(397)
    # specifying the editions
    edition = Filter.EditionFilter([134, 134, 136])
    # making a filter with both filters above
    all_filters = Filter.CompositeFilter([modality, edition])

    # creating ranking
    ranking = FilteredRanking(all_filters, SortByBestMark())

    print('\033[1m' + f"{'Athlete':<25} {'Country':15} {'Edition':20} {'Result (sec)':<5}" + '\033[0m')
    # showing the first 20 results
    for mark in ranking.get_marks()[:20]:
        print(f"{mark.athlete.name:<25} {mark.athlete.country.name:15} {mark.event.edition.name:20} "
              f"{mark.value:<5}")
