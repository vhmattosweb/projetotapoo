"""
Example 2.

Ranking of the male 100m athletics event for athletes from JAM, USA, ITA, and CAN.
"""

from src.core_classes import Filter
from src.Rankings import FilteredRanking
from src.SortStrategy import SortByBestMark


if __name__ == "__main__":
    # choosing the modality
    modality = Filter.ModalityFilter(412)
    # adding the sport filter
    sport = Filter.SportFilter(22)
    # country filter - only marks from athletes from the USA, JAM, ITA, and CAN
    # Ids of each country (409 - USA, 443 - JAM, 414 - ITA, 411 - CAN)
    country = Filter.CountryFilter([409, 443, 414, 411])
    # joinning the filters
    all_filters = Filter.CompositeFilter([modality, sport, country])

    # creating ranking
    ranking = FilteredRanking(all_filters, SortByBestMark())

    print('\033[1m' + f"{'Athlete':<25} {'Country':15} {'Edition':20} {'Result (sec)':<5}" + '\033[0m')
    # showing the first ten results
    for mark in ranking.get_marks()[:10]:
        print(f"{mark.athlete.name:<25} {mark.athlete.country.name:15} {mark.event.edition.name:20} "
              f"{mark.value:<}")
