"""
Example 6 - Shows how to select only marks from a given athlete.
            This ranking is ordered by the best results.
"""

import src.core_classes.Filter as filters
from src.Rankings import FilteredRanking
from src.SortStrategy import SortByBestMark, SortByEdition
from src.Plot import AthleteMarksGraph


if __name__ == "__main__":
    # constructing the filters we need to use to build the ranking
    sport_filter = filters.SportFilter(23)  # id for swimming
    modality_filter = filters.ModalityFilter(408)  # id for 200m medley
    athlete_filter = filters.AthleteFilter([11461])  # Michael Phelps id
    all_filters = filters.CompositeFilter([sport_filter, modality_filter, athlete_filter])

    # creating ranking for the best marks from an athlete
    ranking = FilteredRanking(all_filters, SortByBestMark())
    print("Michael Phelps best marks")
    print('\033[1m' + f"{'Athlete':<25} {'Country':15} {'Edition':20} {'Result (min:sec)':<5}" + '\033[0m')
    for mark in ranking.get_marks():
        print(f"{mark.athlete.name:<25} {mark.athlete.country.name:15} {mark.event.edition.name:20} "
              f"{mark.value:<5}")

    # ranking with the marks sorted by the edition
    ranking.update_strategy(SortByEdition())
    print("\n\nMichael Phelps marks ordered by the edition year")
    print('\033[1m' + f"{'Athlete':<25} {'Country':15} {'Edition':20} {'Result (min:sec)':<5}" + '\033[0m')
    for mark in ranking.get_marks():
        print(f"{mark.athlete.name:<25} {mark.athlete.country.name:15} {mark.event.edition.name:20} "
              f"{mark.value:<5}")

    marks_plot = AthleteMarksGraph(ranking.get_marks())
    marks_plot.plot()
