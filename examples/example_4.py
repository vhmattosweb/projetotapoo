"""
Example 4:

Ranking of male athletes in the 50m freestyle event for the Tokyo 2020, Rio 2016, and London 2012 editions.
"""

from src.core_classes import Filter
from src.Rankings import FilteredRanking
from src.SortStrategy import SortByBestMark, AthleteBestMark


if __name__ == "__main__":
    # chosing the edition
    modality = Filter.ModalityFilter(397)
    # sport filter
    sport = Filter.SportFilter(23)
    # edition filter
    edition = Filter.EditionFilter([134, 135, 136])
    # joinning the filters
    all_filters = Filter.CompositeFilter([modality, edition, sport])

    # creating ranking
    ranking = FilteredRanking(all_filters, AthleteBestMark(SortByBestMark()))

    print('\033[1m' + f"{'Athlete':<25} {'Country':15} {'Edition':20} {'Result (sec)':<5}" + '\033[0m')
    # showing the first 20 results
    for mark in ranking.get_marks()[:20]:
        print(f"{mark.athlete.name:<25} {mark.athlete.country.name:15} {mark.event.edition.name:20} "
              f"{mark.value:<5}")
