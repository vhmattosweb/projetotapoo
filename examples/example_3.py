"""
Example 3:

Ranking of the women's 50m freestyle swimming event, considering only athletes from the USA, SWE, and GRE,
and taking into account the Tokyo, Rio, London, and Beijing editions.
"""

from src.core_classes import Filter
from src.Rankings import FilteredRanking
from src.SortStrategy import SortByBestMark


if __name__ == "__main__":
    # setting up the filters using the right ids
    modality = Filter.ModalityFilter(383)
    edition = Filter.EditionFilter([134, 135, 136, 137])
    sport = Filter.SportFilter(23)
    country_filter = Filter.CountryFilter([409, 404, 428])
    # joinning the filters
    all_filters = Filter.CompositeFilter([modality, sport, country_filter, edition])

    # creating ranking
    ranking = FilteredRanking(all_filters, SortByBestMark())

    print('\033[1m' + f"{'Athlete':<25} {'Country':15} {'Edition':20} {'Result (sec)':<5}" + '\033[0m')
    # showing the first ten results
    for mark in ranking.get_marks():
        print(f"{mark.athlete.name:<25} {mark.athlete.country.name:15} {mark.event.edition.name:20} "
              f"{mark.value:<5}")
